// 51实验板演示程序
// 外部中断学习，巩固
// 数码管左右循环滚动0-9的数字
// 外部中断0控制向左滚动，外部中断1控制向右滚动

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
// 定时器时长，1ms
uchar timer = 114;
// 数码管滚动方向，0-向左，1-向右
uchar direct = 2;
// 普通变量
uchar temp = 0;
uint speed = 0;
uchar i = 0;

// 4位位选信号表
uchar sign[] = {0x07, 0x0B, 0x0D, 0x0E};
// 数码管数字0-9编码表, 0不显示内容
uchar code table[] = {
	/*0xC0,*/
	0xFF,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90
};

// 要显示的内容
//uchar screen[] = {0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
//const uchar length = 12;
uchar screen[] = {0, 0, 0, 1, 2, 3, 4};
const uchar length = 7;
//uchar screen[] = {0, 1, 2, 3};

// 声明延时函数
void delay(uint t);
// 声明滚动内容处理函数
void roll();
// 显示内容
void display();

void main(void)
{
	// EA=1; EX0=1;可以通过IE寄存器设置, IE = 0x81;
	// 开总中断
	EA = 1;
	// 外部中断0触发方式，0-低电平触发，1-下降沿触发 
	// 也可以通过TCON寄存器进行设置 TCON = 0x01;	
	IT0 = 0;
	// 开外部中断0
	EX0 = 1;
	
	// EA=1; EX1=1;可以通过IE寄存器设置, IE = 0x84;
	// 外部中断1触发方式，0-低电平触发，1-下降沿触发
	// 也可以通过TCON寄存器进行设置 TCON = 0x04;	
	IT1 = 0;
	// 开外部中断1
	EX1 = 1;
	
	while(1)
	{
		display();
	}
}

// 外部中断0
void int0() interrupt 0
{
	// 屏蔽连续中断
	EA=0;
	direct = 0;
	EA=1;
}

// 外部中断1
void int1() interrupt 2
{
	// 屏蔽连续中断
	EA=0;
	direct = 1;
	EA=1;
}

// 显示内容
void display()
{
	// 滚动速度
	speed = 20;
	while(speed--)
	{
		// 数码管扫描
		for(i = 0; i < 4; i++)
		{
			// 位选
			P2 = sign[i];
			// 段选
			P1 = table[screen[i]];
			delay(1);
		}
	}
	
	// 处理滚动内容
	roll();
}

// 数组交换滚动内容处理函数
void roll()
{
	// 向左
	if(direct == 0)
	{
		// 相当于向左循环移位
		temp = screen[0];
		for(i = 1; i < length; i++)
		{
			screen[i-1] = screen[i];
		}
		screen[i-1] = temp;
	}
	//向右
	else if(direct == 1)
	{ 
		// 相当于向右循环移位
		temp = screen[length-1];
		for(i = length - 2; i >= 0; i--)
		{
			screen[i+1] = screen[i];
			if(i == 255) {
				break;
			}
		}
		screen[0] = temp;
	}
}

// 简单加减控制内容滚动
//void roll()
//{
//	// 向左
//	if(direct == 0)
//	{
//		// 相当于向左循环移位
//		for(i = 0; i < 4; i++)
//		{
//			if(screen[i] == 9)
//			{
//				screen[i] = 0;
//			} else {
//				screen[i]++;
//			}
//		}
//	} 
//	//向右
//	else 
//	{ 
//		// 相当于向右循环移位
//		for(i = 0; i < 4; i++)
//		{
//			if(screen[i] == 0)
//			{
//				screen[i] = 9;
//			} else {
//				screen[i]--;
//			}
//		}
//}

// 延时函数
void delay(uint t)
{	
	while(t--)
	{
		timer = 114;
		while(timer--);
	}
}
