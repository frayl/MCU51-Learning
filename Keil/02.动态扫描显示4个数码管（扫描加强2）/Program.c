// 51实验板数码管显示
// 共阳极模式
// 动态扫描显示4个数码管
// 每隔Xm秒显示一个4位数字

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

uchar timer = 0;
uchar tm = 0;
uchar n = 0;

//数码管位选信号
uchar turbo[] = {
	0x07, 0x0B, 0x0D, 0x0E
};
// 显示内容
uchar value[] = {0, 0, 0, 0};
// 数码管编码表0-9
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90
};
// 要显示的数字
uint code number[] = {
	1111, 2222, 3333, 4444, 
	5555, 6666, 7777, 8888, 
	9999, 1234, 2345, 3456, 
	4567, 5678, 6789, 7890
};
// over的数码管编码
uint code ch[] = {
	0xC0,0xC1,0x84,0x8D
};

// 声明函数
void delay(uint t);

void main(void)
{
	// 初始化数码管
	P2 = 0;
	P1 = table[0];
	P2 = 0x0F;
	
	do{
		//if(n == 16) n = 0;
		
		// 分离出千位，百位，十位，各位
		value[0] = number[n] / 1000;
		value[1] = (number[n] % 1000) / 100;
		value[2] = ((number[n] % 1000) % 100) / 10;
		value[3] = number[n] % 10;
		
		// 动态扫描X次就轮换下个数字		
		tm = 500;
		while(tm--)
		{
			// 显示各个位上的数字
			P2 = turbo[0];
			P1 = table[value[0]];
			delay(1);
			// 第2位
			P2 = turbo[1];
			P1 = table[value[1]];
			delay(1);						
			// 第3位
			P2 = turbo[2];
			P1 = table[value[2]];
			delay(1);
			// 第4位
			P2 = turbo[3];
			P1 = table[value[3]];
			delay(1);
		}
	}while(++n < 16);
	
	//  显示over字符
	while(1)
	{
		// 显示各个位上的数字
		P2 = turbo[0];
		P1 = ch[0];
		delay(1);
		// 第2位
		P2 = turbo[1];
		P1 = ch[1];
		delay(1);						
		// 第3位
		P2 = turbo[2];
		P1 = ch[2];
		delay(1);
		// 第4位
		P2 = turbo[3];
		P1 = ch[3];
		delay(1);
	}
}

// 延时x毫秒
void delay(uint t)
{	
	while(t--)
	{
		timer = 114;
		while(timer--);
	}
}
