// 51实验板数码管显示
// 共阳极模式
// 动态扫描显示4个数码管(加强版)

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

uchar timer = 114;
uint n = 0;

//数码管位选信号
uchar turbo[] = {
	0x0E, 0x0D, 0x0B, 0x07
};
// 显示内容
uchar value[] = {0, 0, 0, 0};
// 数码管编码表
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};

// 声明函数
void delay(uint t);

void main(void)
{
	// 初始化数码管
	P2 = 0;
	P1 = table[0];
	P2 = 0x0F;
	
	while(1)
	{
		// 第1位数码管
		value[3] = 0;
		do
		{			
			// 第2位数码管
			value[2] = 0;
			do
			{
				// 第3位数码管
				value[1] = 0;
				do
				{					
					// 第4位数码管
					value[0] = 0;
					do
					{
						// 动态扫描显示
						n = 200;
						while(n--)
						{
							// 第1位
							P2 = turbo[3];
							P1 = table[value[3]];
							delay(1);
							// 第2位
							P2 = turbo[2];
							P1 = table[value[2]];
							delay(1);						
							// 第3位
							P2 = turbo[1];
							P1 = table[value[1]];
							delay(1);
							// 第4位
							P2 = turbo[0];
							P1 = table[value[0]];
							delay(1);
						}
					}while(++value[0] < 16);
				}while(++value[1] < 16);
			}while(++value[2] < 16);
		}while(++value[3] < 16);
	}
}

// 延时x毫秒
void delay(uint t)
{	
	while(t--)
	{
		timer = 114;
		while(timer--);
	}
}
