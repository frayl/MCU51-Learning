#include <REGX52.H>

// 函数原型  
void SystemInit(void);  
void Delay_840us(void);  
void Delay_2400us(void);  
void LedDisp();  
unsigned char GetCode(void);//获得码  
void  delay(unsigned char loop);  
  
// 位变量  
sbit IRIN = P3^2;
sbit BEEP = P1^6;
sbit swch = P1^7;
  
// 变量  
unsigned char KeyValue;     //机器码  
unsigned char MaValue;      //键值码；  
unsigned char disbuf[4];    //数码管显示缓冲  
unsigned char scan[4]={0xFE,0xFD,0xFB,0x07}; //p2位选择

unsigned char code table[16] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};  
  
/** 
 * 延时 
 */  
void delay(unsigned char loop)   
{    
    unsigned char i;
    for(i=0;i<loop;i++);
    TR1=1;   
    while(!TF1);   
    TF1=0;
    TR1=0;
}  
  
/** 
 * 延时9ms  
 */  
void Delay_9000us(void)  
{  
    TL1 = 153.6;
    TH1 = 223.6;
    TR1 = 1;  
    while(!TF1);  
    TF1 = 0;  
    TR1 = 0;  
}  
  
/** 
 * 延时4.5ms  
 */  
void Delay_4500us(void)  
{  
    TH1 = 239.8;  
    TL1 = 204.8;  
    TR1 = 1;  
    while(!TF1);  
    TF1 = 0;  
    TR1 = 0;  
}  
  
/** 
 * 系统初始化  
 */  
void SystemInit(void)  
{  
    IRIN = 1;  
    IT0 = 1;         //INT0负跳变触发  
    TMOD = 0x10;     //定时器1工作在方式1  
    EA = 1;  
    EX0 = 1;  
}  
  
/** 
 * 读码  
 */  
unsigned char GetCode()  
{   
    unsigned char n;  
  
    static temp = 0;   
  
    for( n = 0; n < 8; n++ )   
    {  
        while(!IRIN);  // 等待高电平，开始解码  
  
        Delay_840us(); // 延时0.84ms  
  
        if(IRIN) // 若仍然为高电平，则为1，否则为0  
        {  
           temp = (0x80|(temp>>1));  // 1    
           while(IRIN); //等待跳变成低电平  
        }  
        else {   
            temp=(0x00|(temp>>1));  // 0  
        }  
    }   
  
    return temp;  
}  
  
/** 
 * 数码管显示 
 */  
void LedDisp()  
{   
    unsigned char i, t;  
    for(i=0;i<4;i++)  
    {
		for(t = 100; t > 0; t--)
		{
			P0=table[disbuf[i]];  
			P2 = scan[i];
			// delay(50);  
			// P0=0x00;
		}
    }  
}  
  
void main(void)  
{   
    SystemInit();  
  
    while(1)   
    {    
        //以下是查表显示  
        disbuf[0]=(((KeyValue&0xf0)>>4)&0x0f);  
        disbuf[1]=KeyValue&0x0f;  
        disbuf[2]=(((MaValue&0xf0)>>4)&0x0f);  
        disbuf[3]=MaValue&0x0f;    
        LedDisp();
    }  
}  
  
  
void interr_ir(void) interrupt 0  
{   
    /** 
     * 用户码和机器码  
     */  
    unsigned char addrl,addrh,num1,num2;   
  
    EA = 0;  //先关闭外部中断0  
  
    Delay_9000us(); // 检测9ms开始码  
  
    if (IRIN) {     // 检测是否为干扰信号  
        EA = 1;     // 重新开启外部中断0  
        return ;    // 退出解码  
    }  
             
    while(!IRIN);   // 等待跳为高电平  
  
    Delay_4500us(); // 检测4.5ms结果码  
  
    if (IRIN) {     // 检测是否为干扰信号  
        EA = 1;     // 重新开启外部中断0  
        return ;    // 退出解码  
    }  
  
    // 读码  
    addrl=GetCode(); // 用户编码高位  
    addrh=GetCode(); // 用户编码低位  
    num1=GetCode();  // 机器码  
    num2=GetCode();  // 机器码反码  
  
    //校验是否为错码  
    if(num1!=~num2)  
    {   
        KeyValue=14;   
        EA=1;   
        return;  
    }  
  
    KeyValue=num2;  
    MaValue=addrh;    
  
    EA=1;  
} 