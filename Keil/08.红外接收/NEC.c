/****************************************
*@文件: NEC.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-25
*@描述: 采用NEC协议的红外接收器
*@接线: P0口控制7段数码管的段显示
*       P2.0-P2.3控制数码管的位选择
*       P3.2用于红外信号传输
****************************************/

#include <REGX52.H>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;
// 只用到4位七段数码管的第一位
//#define SEG 0xFE;

// 红外接收头数据线
sbit IRIN = P3^2;
// 接收到的数据
uchar GETCODE[4] = {0x00, 0x00, 0x00, 0x00};

// 全局变量
// LED显示字模 0-F 共阳模式
uchar code table[]= {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};
// 函数声明
void delay(uint);
void display(void);

/****************************************
*@描述: 主函数
****************************************/
void main(void)
{
//	// 定时器1工作于8位自动重载模式, 用于产生波特率
//	TMOD = 0x20;
//	// 波特率9600
//	TH1 = TL1 = 0xFD;
//	// 设定串行口工作方式
//	SCON = 0x50;
//	// 波特率不倍增
//	PCON &= 0xEF;
//	// 启动定时器1
//	TR1 = 1;
	
	// 初始化外部中断0
	EX0 = 1;
	// 负边沿触发
	TCON = 0x01;
	// 打开中断开关
	EA = 1;
	// 
	IRIN = 1;
	while(1)
	{
		display();
	}
}

/****************************************
*@描述: 7段数码管字符显示
*@返回: void
****************************************/
void display(void)
{
	uchar k1, k2, k3, k4, n;
	
	// 显示的时候禁止中断
	EA = 0;
	
	k1 = (GETCODE[2] >> 4) & 0x0F;
	k2 = GETCODE[2] & 0x0F;
	k3 = (GETCODE[1] >> 4) & 0x0F;
	k4 = GETCODE[1] & 0x0F;
	
	// 显示高位
	for(n = 0; n < 10; n++)
	{
		P2 = 0xFD;
		P0 = table[k1];
	}
	
	// 显示低位
	for(n = 0; n < 10; n++)
	{
		P2 = 0xFE;
		P0 = table[k2];
	}
	
	// 地址位
	for(n = 0; n < 10; n++)
	{
		P2 = 0xFB;
		P0 = table[k3];
	}
	
	// 地址位
	for(n = 0; n < 10; n++)
	{
		P2 = 0xF7;
		P0 = table[k4];
	}
	
	// 显示完毕打开中断
	EA = 1;
}

/****************************************
*@描述: 外部中断0中断函数
*@返回: void
****************************************/
void int0() interrupt 0 using 1
{
	uchar n, i;
	// 关闭中断，防止重复触发
	EX0 = 0;
	// 延时3ms
	delay(490);
	// 不是有效的信号，丢弃
	if(IRIN == 1)
	{
		EX0 = 1;
		return;
	}
	
	// 跳过9ms的引导码
	while(!IRIN);
	// 跳过4.5ms的起始码, 延时3ms
	delay(490);
	// while(IRIN);
		
	// 接收4组数据
	for(n = 0; n < 4; n++)
	{
		// 每组数据的8位
		for(i = 0; i < 8; i++)
		{
			// 等待低电平
			while(!IRIN);
			// 延时500微秒，判断是0还是1
			// 为0高电平维持0.565毫秒
			// 为1高电平维持1.685毫秒
			// 跳过500微秒，要么还是高电平，要么已经是下一个位的低电平了
			delay(90);
			if(IRIN == 0)
			{
				// 什么也不做
			}
			else
			{
				// 低位在前，高位在后
				GETCODE[n] = GETCODE[n] | (0x01 << i);
				// 等待跳变为低电平
				while(IRIN);
			}
		}
	}
	
	// 校验是否为错码
	if(GETCODE[2] != ~GETCODE[3])
	{
		GETCODE[0] = GETCODE[1] = GETCODE[2] = GETCODE[3] = 0;
		EX0 = 1;
		return;
	}
	
	// 通过串口发送收到的内容
//	for(n = 0; n < 4; n++)
//	{
//		SBUF = GETCODE[n];
//		while(!TI);
//		TI = 0;
//	}
	
	// 重开中断，等待下一次接收数据
	EX0 = 1;
}

/****************************************
*@描述: 微秒延时函数,
*       此函数精确计算：18+6*(t-1)=延时时间(us)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}
