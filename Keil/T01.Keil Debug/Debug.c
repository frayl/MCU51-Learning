#include<stdio.h>
#include<reg52.h>

sbit p1_1 = P1^0;

unsigned int t;

void main(void)
{
	p1_1 = 0;
	//t = 0xFF;
	
	t = 1;
	while(t--); // 最小执行时间1.1微秒
	t = 1;
	
	while(1)
	{
		p1_1 = ~p1_1;
		t = 8300;  //  通过调试得知8300为大约100毫秒（0.1秒）的时间，则可得出83为1毫秒的时间
		while(t--);
		//printf("%d, %ud", t, t);
	}
}