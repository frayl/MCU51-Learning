// 51实验板演示程序
// 定时器0中断演示
// 本程序使用了共阳极数码管
// 通过定时器中断实现数码管的动态显示

#include <REGX52.H>

#define uchar unsigned char
#define uint unsigned int

uchar num = 0;
uchar time = 0;
	

// 共阳极数码管编码表
uchar code table[] = {
	0xc0,0xf9,0xa4,0xb0,
	0x99,0x92,0x82,0xf8,
	0x80,0x90,0x88,0x83,
	0xc6,0xa1,0x86,0x8e
};

void main(void)
{
	// 定时计数器初值
	TH0 = (65536 - 10000) / 256;
	TL0 = (65536 - 10000) % 256;
	
	// 中断总开关
	EA = 1;
	// 定时器0中断允许开关
	ET0 = 1;
	//设置定时器0为工作方式1
	TMOD = 0x01;
	// 启动定时器0
	TR0 = 1;
	
	while(1);
}

// 定时10m秒
void timer0() interrupt 1
{
	// 定时计数器初值
	TH0 = (65536 - 10000) / 256;
	TL0 = (65536 - 10000) % 256;
	
	time++;
	
	if(time == 100)
	{
		time = 0;
		if(num > 15) num = 0;
		
		P1 = table[num++];
	}
}
