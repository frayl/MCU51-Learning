/****************************************
*@文件: Eledriven.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-15
*@描述: 51实验板程序
*       口控制ULN2003L来驱动直流电机的正反转
*       按键UP控制电机正转，DOWN键控制电机反转
*@接线: P00->MOT1, P01->MOT2, P02->UP, P03->LEFT
*       P04->RIGHT, P05->DOWN
*       P06->LED灯(按键时亮灯，可以不接)
*       P07->LED灯(每一秒亮一次灯，可用来测试中断状态，可不接)
*       电机接WX1和WX2
*@调试结果: ULN2003不能驱动电机正反转，只能实现正传
*           网上说L298N可以驱动，但未实验
****************************************/

#include <REGX52.H>

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

#define STOP 0	// 停止
#define UP 1    // 持续正转
#define LEFT 2  // 正转1次
#define RIGHT 3 // 反转1次
#define DOWN 4  // 持续反转
#define MAX 9   // 最大占空比
#define MIN 1   // 最小占空比
#define STEP 1  // 增减占空比步长

sbit TO_LEFT = P0^0;
sbit TO_RIGHT = P0^1;
sbit KEY_UP = P0^2;
sbit KEY_LEFT = P0^3;
sbit KEY_RIGHT = P0^4;
sbit KEY_DOWN = P0^5;
sbit KEY_LED = P0^6;
sbit COUNTER_LED = P0^7;

uchar DIRECT = 0;
uchar SPACE = 5;
uchar COUNT = 0;
uchar TIMER = 0;

// 函数申明
void delay(uint);
void key_scan(void);

void main(void)
{
	// 初始化定时器T0 
	TH0 = 0xDB;
	TL0 = 0xFF;
	TMOD = 0x01;
	ET0 = 1;
	EA = 1;
	TR0 = 1;
	
	// 按键出入初始化
	KEY_UP = 1;
	KEY_LEFT = 1;
	KEY_RIGHT = 1;
	KEY_DOWN = 1;

	while(1)
	{
		key_scan();
		//if(DIRECT != STOP)
		//{
		//	KEY_LED = 0;
		//}
	}
}

/****************************************
*@功能: 定时器T0中断函数
*@描述: 精确计时10毫秒
*@参数: void
****************************************/
void timer0() interrupt 1
{
	//TR0 = 0;
	TH0 = 0xDB;
	TL0 = 0xFF;
	
	COUNT++;
	TIMER++;
	
	if(COUNT == 10)
	{
		COUNT = 0;
	}
	
	if(TIMER == 100)
	{
		COUNTER_LED = 0;
		TIMER = 0;
	} 
	else 
	{
		COUNTER_LED = 1;
	}
	
	// 
	if(COUNT < SPACE)
	{
		// 转动
		switch(DIRECT)
		{
			case UP:
				TO_LEFT = 1;
				TO_RIGHT = 0;
				break;
			case LEFT:
				TO_LEFT = 1;
				TO_RIGHT = 0;
				// 关闭电动机
				DIRECT = 0;
				break;
			case RIGHT:
				TO_LEFT = 0;
				TO_RIGHT = 1;
				// 关闭电动机
				DIRECT = 0;
				break;
			case DOWN:
				TO_LEFT = 0;
				TO_RIGHT = 1;
				break;
			default:
				TO_LEFT = 0;
				TO_RIGHT = 0;
				break;
		}
	}

	// 占空位
	if(COUNT >= SPACE)
	{
		TO_LEFT = 0;
		TO_RIGHT = 0;
	}
	
	//TR0 = 1;
}

/****************************************
*@功能: 按键检测函数
*@描述: 检测是否有键被按下
*@参数: void
****************************************/
void key_scan(void)
{
	// UP键按下
	if(KEY_UP == 0)
	{
		// 延时5ms
		delay(45);
		// 确实被按下
		if(KEY_UP == 0)
		{
			SPACE += STEP;
			if(SPACE > MAX) SPACE = MAX;
			
			KEY_LED = 0;
			while(KEY_UP == 0);
			KEY_LED = 1;
			
			DIRECT = UP;
		}
	}

	// LEFT键按下
	if(KEY_LEFT == 0)
	{
		// 延时5ms
		delay(45);
		// 确实被按下
		if(KEY_LEFT == 0)
		{
			KEY_LED = 0;
			while(KEY_LEFT == 0);
			KEY_LED = 1;
			
			DIRECT = LEFT;
		}
	}

	// RIGHT键按下
	if(KEY_RIGHT == 0)
	{
		// 延时5ms
		delay(45);
		// 确实被按下
		if(KEY_RIGHT == 0)
		{
			KEY_LED = 0;
			while(KEY_RIGHT == 0);
			KEY_LED = 1;
			
			DIRECT = RIGHT;
		}
	}

	// DOWN键按下
	if(KEY_DOWN == 0)
	{
		// 延时5ms
		delay(45);
		// 确实被按下
		if(KEY_DOWN == 0)
		{
			SPACE -= STEP;
			if(SPACE < MIN) SPACE = MIN;
			
			KEY_LED = 0;
			while(KEY_DOWN == 0);
			KEY_LED = 1;
			
			DIRECT = DOWN;
		}
	}
	
	//DIRECT = STOP;
}

/****************************************
*@功能: 延时函数
*@描述: 最小执行时间为110微秒
*@参数: t, 执行时长
****************************************/
void delay(uint t)
{
	uint time;
	while(t--)
	{
		// 如果晶振是11.0592MHZ，则执行时间1.1微秒
		for(time = 100; time > 0; time--);
	}
}
