// 51实验板数码管显示
// 共阳极模式

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
	
sbit K1 = P2^0;
sbit K2 = P2^1;
sbit K3 = P2^2;
sbit K4 = P2^3;
uchar timer;
uint n = 0;

/*
// 测试，点亮第一个数码管
void main()
{
	K1 = 0;
	P1 = 0x86;
}
*/
uchar value[] = {0, 0, 0, 0};

uchar code table[] = {
	0x8E,0xC0,0xF9,0xA4,
	0xB0,0x99,0x92,0x82,
	0xF8,0x80,0x90,0x88,
	0x83,0xC6,0xA1,0x86,
};

// 声明函数
void delay(uint t);

void main(void)
{
	// 初始化数码管
	K1 = K2 = K3 = K4 = 0;
	P1 = table[0];
	K1 = K2 = K3 = K4 = 1;
	
	while(1)
	{
		// 第四段数码管
		value[3] = 0;
		while(value[3] < 16)
		{
			// 第四段数码管开关
			//K4 = 0;
			//P1 = table[value[3]];
			//K4 = 1;
			value[3]++;
			
			// 第三段数码管
			value[2] = 0;
			while(value[2] < 16)
			{
				// 第三段数码管开关
				//K3 = 0;
				//P1 = table[value[2]];
				//K3 = 1;
				value[2]++;
				
				// 第二段数码管
				value[1] = 0;
				while(value[1] < 16)
				{
					// 第二段数码管开关
					//K2 = 0;
					//P1 = table[value[1]];
					//K2 = 1;
					value[1]++;
					
					value[0] = 0;
					while(value[0] < 16)
					{
						// 扫描数码管
						n = 100;
						while(n--)
						{
							// 第四段
							K1 = K2 = K3 = 1;
							K4 = 0;
							P1 = table[value[3]];
							delay(1);
							// 第三段
							K1 = K2 = K4 = 1;
							K3 = 0;
							P1 = table[value[2]];
							delay(1);						
							// 第二段
							K1 = K3 = K4 = 1;
							K2 = 0;
							P1 = table[value[1]];
							delay(1);
							// 第一段
							K2 = K3 = K4 = 1;
							K1 = 0;
							P1 = table[value[0]];							
							delay(1);
						}
						value[0]++;
						
						//delay(50);
					}
				}
			}
		}
	}
}

// 延时x毫秒
void delay(uint t)
{
	while(t--)
	{
		for(timer = 114; timer > 0; timer--);
	}
}
