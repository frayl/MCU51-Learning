// 51实验板演示程序
// 外部中断实现
// 循环显示数码管
// 外部中断0触发的LED灯的亮
// 外部中断1触发的LED灯的灭

#include <REGX52.H>

#define uchar unsigned char
#define uint unsigned int
	
sbit D1 = P2^0;
uchar num = 0;
const uint time = 114;

// 共阳极数码管编码表
uchar code table[] = {
	0xc0,0xf9,0xa4,0xb0,
	0x99,0x92,0x82,0xf8,
	0x80,0x90,0x88,0x83,
	0xc6,0xa1,0x86,0x8e
};

void delay(uint t);

void main(void)
{
	// 开中断总开关
	EA = 1;
	// 外部中断0控制方式, 0-低电平触发，1-下降沿触发
	IT0 = 0;
	// 外部中断0开关
	EX0 = 1;
	// 外部中断1控制方式, 0-低电平触发，1-下降沿触发
	IT1 = 0;
	// 外部中断1开关
	EX1 = 1;
	
	while(1)
	{
		num = 0;
		do
		{
			P1 = table[num];
			delay(1000);
			
		}while(++num < 16);
	}
}

// 外部中断0触发事件
void Interrupt0() interrupt 0
{
	D1 = 0;
}

// 外部中断1触发事件
void Interrupt1() interrupt 2
{
	D1 = 1;
}

// 延时X毫秒
void delay(uint t)
{
	t = t * time;
	while(t--);
}