/****************************************
*@文件: 38KHz1.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-26
*@描述: 延时法产生38KHz载波演示
*@接线: P1.0输出载波脉冲
****************************************/

#include <REG1051.H>
#include <intrins.h>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

#define HH 1
#define LL 0

sbit WAVE = P1^0;
sbit KEY = P3^2;
bit STATE = LL;

// 全局变量

// 函数声明
void delay(uint);
void makeWave(void);
void makeSignal(void);

/****************************************
*@描述: 
*@参数: 
*@返回:
****************************************/
void main(void)
{	
	// 设置为输入口
	KEY = 1;
	
	while(1)
	{
		if(KEY == 0)
		{
			// 按键消抖
			delay(900);
			if(KEY == 0)
			{
				// 按键弹起
				while(!KEY);
				
				makeWave();
			}
		}
	}
}

/****************************************
*@描述: 仿NEC编码发生器
*@参数: void
*@返回: void
****************************************/
void makeWave(void)
{
	uchar n = 0;
	uint t;
	
	// 9ms引导码
	WAVE = LL;
	STATE = LL;
	for(t = 0; t < 346; t++)
	{
		makeSignal();
	}

	// 4.5ms起始码
	WAVE = HH;
	STATE = HH;
	for(t = 0; t < 173; t++)
	{
		makeSignal();
	}
	
	for(n = 0; n < 32; n++)
	{
		// 输出高电平
		WAVE = LL;
		STATE = LL;
		// 延时650us
		for(t = 0; t < 25; t++)
		{
			makeSignal();
		}
		
		// 输出低电平
		WAVE = HH;
		STATE = HH;
		// 延时1685us
		for(t = 0; t < 64; t++)
		{
			makeSignal();
		}
	}
	
	// 结束码,延时2.5ms
	WAVE = LL;
	STATE = LL;
	for(t = 0; t < 96; t++)
	{
		makeSignal();
	}
	WAVE = HH;
	STATE = HH;
}

/****************************************
*@描述: 产生17/9的PWM脉冲
*       38KHz载波每个波长的周期为26us
*       11.0592MHz的单片机，每个机器周期为1.08us
*@参数: void
*@返回: void
****************************************/
void makeSignal(void)
{	
	uchar i;
	
	if(STATE == HH)
	{
		for(i = 0; i < 14; i++);
		return;
	}
	else
	{
		// 跳过16个高电平
		for(i = 0; i < 8; i++);
		
		WAVE = HH;
		for(i = 0; i < 4; i++);
		
		WAVE = STATE;
		return;
	}
}

/****************************************
*@描述: 微秒延时函数, Time=18+6(t-1)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}
