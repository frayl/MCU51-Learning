/****************************************
*@文件: 38KHz.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-26
*@描述: 定时中断法产生38KHz载波演示
*@接线: P1.0输出载波脉冲
****************************************/

#include <REG1051.H>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

#define HH 1
#define LL 0

sbit WAVE = P1^0;
sbit KEY = P3^2;
bit STATE = 1;

// 全局变量

// 函数声明
void delay(uint);
void makeWave(void);

/****************************************
*@描述: 
*@参数: 
*@返回:
****************************************/
void main(void)
{
	// 初始化定时器, 8位自动重装定时器
	TMOD = 0x02;
	// 38KHz的波长周期为26us, 延时17秒，产生17/9的PWM脉冲
	TH0 = TL0 = 0xEE; //0xEE
	// 打开定时器开关
	ET0 = 1;
	//TR0 = 1;
	// 设置定时器中断优先
	//IP |= 0x02;
	
	// 初始化外部中断0, 低电平触发
	//IT0 = 1;
	//EX0 = 1;
	
	// 开中断
	EA = 1;
	
	while(1)
	{
		KEY = 1;
		if(KEY == 0)
		{
			delay(900);
			if(KEY == 0)
			{
				while(!KEY);
				TR0 = 1;
				makeWave();
				TR0 = 0;
			}
		}
	}
}

/****************************************
*@描述: 仿NEC编码发生器
*@参数: void
*@返回: void
****************************************/
void makeWave(void)
{
	uchar n = 0;
	
	// 9ms引导码
	WAVE = LL;
	STATE = LL;
	delay(1498);
	// 4.5ms起始码
	WAVE = HH;
	STATE = HH;
	delay(748);
	
	for(n = 0; n < 32; n++)
	{
		// 输出高电平
		WAVE = LL;
		STATE = LL;
		// 延时650us
		delay(157);
		// 输出低电平
		WAVE = HH;
		STATE = HH;
		// 延时1658us
		delay(274);
	}
	
	// 结束码,延时2.5ms
	WAVE = LL;
	STATE = LL;
	delay(415);
	WAVE = HH;
	STATE = HH;
}

/****************************************
*@描述: 外部中断0中断函数
*@参数: void
*@返回: void
****************************************/
//void int0(void) interrupt 0 using 1
//{
//	TR0 = 1;
//	makeWave();
//	TR0 = 0;
//}

/****************************************
*@描述: 定时器T0中断函数, 产生17/9的PWM脉冲
*@参数: void
*@返回: void
****************************************/
void timer0(void) interrupt 1 using 1
{	
	uchar i;
	
	if(STATE == LL)
	{
		WAVE = HH;		
	}
	else
	{
		i = 0;
	}
	for(i = 0; i < 2; i++);
	WAVE = STATE;
}

/****************************************
*@描述: 微秒延时函数, Time=18+6(t-1)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}
