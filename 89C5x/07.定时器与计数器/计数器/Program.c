// 标题：计数器仿真实验
// 材料：51单片机，共阳极4段数码管，数字时钟发生器
// 功能：数字时钟产生方波给计数器T0产生计数中断，累积的数值在数码管上显示

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

// 片选信号
uchar code sign[] = {0x01, 0x02, 0x04, 0x08};

// 共阳极数码管0-9编码
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90
};

// 当前显示的内容
uchar screen[] = {0, 0, 0, 0};

// 1毫秒执行的指令数
uchar times = 114;
// 计数器累加值
uint num = 0;
uint count = 0;
// 
uint temp = 0;
uchar n = 0;


// 声明延时函数
void delay(uint t);

void main(void)
{
	// 设置计数器初值, 每个脉冲计数一次
	TH0 = 0xFF;
	TL0 = 0xFE;
	// 设置T0为计数器模式
	TMOD = 0x05;
	// 开计数器中断
	ET0 = 1;
	// 开始计数
	TR0 = 1;
	// 开总中断
	EA = 1;

	while(1)
	{
		// 分离出千位、百位、十位、个位
		// 千位
		n = count / 1000;
		screen[0] = table[n];
		// 百位
		n = (count % 1000) / 100;
		screen[1] = table[n];
		// 十位
		n = ((count % 1000) % 100) / 10;
		screen[2] = table[n];
		// 个位
		n = count % 10;
		screen[3] = table[n];
		
		// 每位显示时长
		num = 20;
		while(num--)
		{
			// 扫描数码管
			// 扫描显示的时候，不要进行任何其它操作，否则显示会乱
			for(n = 0; n < 4; n++)
			{
				P2 = sign[n];
				P1 = screen[n];
				delay(1);
			}
		}
	}
}

// 计数器T0中断函数
void counter0() interrupt 1
{	
	EA = 0;
	TH0 = 0xFF;
	TL0 = 0xFE;
	
	count++;
	if(count >= 9999)
	{
		count = 0;
	}
	EA = 1;
}

// 延时函数
void delay(uint t)
{	
	while(t--)
	{
		times = 114;
		while(times--);
	}
}
		   