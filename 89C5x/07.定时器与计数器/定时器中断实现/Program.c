// 定时器中断控制数码管的显示
// 外部中断控制数码管显示位的跳跃

#include <REGX52.H>

#define uchar unsigned char
#define uint unsigned int
	
// 4位位选信号表
uchar sign[] = {0x0E, 0x0D, 0x0B, 0x07};
// 数码管数字0-9编码表
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90
};

uint time = 0;
// 当前位
uchar position = 0;
// 当前显示值
uchar num = 0;

void main(void)
{
	// 定时器中断设置
	// 设置定时器工作模式, 16位定时器
	TMOD = 0x01;
	// 
	TH0 = 0xD8;
	TL0 = 0xF0;
	// 设置为定时器模式
	ET0 = 1;	
	// 开始定时
	TR0 = 1;
	
	// 外部中断0设置
	IT0 = 0;
	EX0 = 1;
	
	// 开总中断
	EA = 1;
	
	P2 = sign[position];
	P1 = table[num];
	while(1);
}


// 外部中断程序
void int0() interrupt 0
{
	// 防止连续中断
	EA = 0;
	
	++position;
	
	position %= 4;
	
	EA = 1;
}

// 定时器中断程序
void timer0() interrupt 1
{
	// 关定时器
	TR0 = 0;
	// 每10毫秒中断一次
	TH0 = 0xD8;
	TL0 = 0xF0;
	
	TR0 = 1;
	
	time++;
	
	if(time == 100)
	{
		if(num == 10) num = 0;
		
		P2 = sign[position];
		P1 = table[num];
		
		time = 0;
		num++;
	}
}
