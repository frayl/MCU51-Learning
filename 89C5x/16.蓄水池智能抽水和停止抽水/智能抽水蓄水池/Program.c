// 功能：智能蓄水池
// 平台：Proteus
// 器材：AT89C52,开关（模拟缺水、水满），LED（指示缺水、水满状态），非门,或门，NPN三极管，继电器，等
// 实现：缺水（SW1断开，触发外部中断0，继电器工作、开始抽水）
//       水满（SW1和SW2都闭合，触发外部中断1，继电器停止工作，抽水停止）

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

sbit Work = P1^0;

void main(void)
{
	// 设置外部中断0工作方式为低电平触发
	IT0 = 0;
	// 开外部中断0
	EX0 = 1;
	// 设置外部中断1工作方式为低电平触发
	IT1 = 0;
	// 开外部中断1
	EX1 = 1;
	// 打开中断开关
	EA = 1;
	
	while(1);
}

// 外部中断0函数
void int0() interrupt 0
{
	// 关闭中断，防止重复中断
	EA = 0;
	// 启动继电器工作
	Work = 0;
	// 开中断
	EA = 1;
}

// 外部中断1函数
void int1() interrupt 2
{
	EA = 0;
	Work = 1;
	EA = 1;
}
