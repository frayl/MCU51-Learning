// 非编码键盘按键检测
// 学习按钮检测，处理按键抖动问题，并将检测到的按键显示在数码管上
//  键盘接P0口，行带上拉电阻

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

// 行列输出信号
uchar code line[] = {
	0xEF, 0xDF, 0xBF, 0x7F
};

// 数码管编码0-F
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};

// 每毫秒执行的指令数
uchar seconds = 0;
uchar scan = 0;
uchar temp = 0;
uchar key = 0;
uchar col = 0;
//uchar row = 0;

// 声明按键扫描函数
uchar keyscan();
// 声明延时函数
void delay(uint t);

///////////////////////
void main(void)
{
	P2 = 0x01;
	while(1)
	{		
		key = keyscan();		
		P1 = table[key];
		delay(5);
	}
}

// 按键扫描
uchar keyscan()
{	
	// 给列输出低电平，行输出高电平
	P0 = 0xF0;
	// 检测是否有键按下
	if((P0 & 0xF0) != 0xF0)
	{
		// 延时5ms，消除按键抖动
		delay(5);		
		// 消除抖动后，确实有按键按下
		if((P0 & 0xF0) != 0xF0)
		{
			// 确定行的位置
			scan = P0 & 0xF0;
						
			// 逐行改变输出信号，列接受信号
			for(col = 0; col < 4; col++)
			{
				P0 = line[col];
				// 判断列的位置
				if((P0 & 0x0F) != 0x0F)
				{
					temp = P0 & 0x0F;
					// 合并行列值
					scan = scan | temp;
				}
			}
		}
	}
	
	//获得按键按钮序号
	switch(scan)
	{
		case 0xEE:
			return 0;
			break;
		case 0xED:
			return 1;
			break;
		case 0xEB:
			return 2;
			break;
		case 0xE7:
			return 3;
			break;
		case 0xDE:
			return 4;
			break;
		case 0xDD:
			return 5;
			break;
		case 0xDB:
			return 6;
			break;
		case 0xD7:
			return 7;
			break;
		case 0xBE:
			return 8;
			break;
		case 0xBD:
			return 9;
			break;
		case 0xBB:
			return 10;
			break;
		case 0xB7:
			return 11;
			break;
		case 0x7E:
			return 12;
			break;
		case 0x7D:
			return 13;
			break;
		case 0x7B:
			return 14;
			break;
		case 0x77:
			return 15;
			break;
		default:
			return 16;
			break;
	}
}

// 延时函数
void delay(uint t)
{	
	while(t--)
	{
		seconds = 114;
		while(seconds--);
	}
}
