// 功能：串口调试程序
// 测试平台：Proteus
// 用途：用于学习串口通讯
// 实现方法：单片机不断向串口发送字符，串口调试器接收字符

#include <REGX52.H>

// 定义波特率选择寄存器
//sfr SMOD = 0x87;

void delay();

void main(void)
{
	unsigned char temp = 0;
	
	// 设置定时器T1的工作模式,8位自动重装定时器/计数器
	TMOD = 0x20;
	// 串口工作方式, 10位UART
	SCON = 0x50;
	// 通信波特率倍增
	//SMOD = 1;
	// 设置计数器初值,晶振平率11.0592MHZ
	TH1 = TL1 = 0xFD;
	// 开定时器中断
	TR1 = 1;
	
	// 扫描接收中断标志RI
	while(1)
	{
		SBUF = 'C';
		while(!TI);
		delay();
		TI=0;
	}
}

void delay()
{
	unsigned char t = 0;
	unsigned int i = 0;
	for(i = 0; i < 10; i++)
	{
		t = 144;
		while(t--);
	}
}
