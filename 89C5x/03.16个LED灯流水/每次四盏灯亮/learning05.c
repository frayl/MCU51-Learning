#include <REGX52.H>

#define unit unsigned int
#define uchar unsigned char
	
sbit p2_0 = P2^0;
sbit p2_1 = P2^1;
unit state = 0x0FFF;	// 每次四盏灯亮
unit temp = 0;
uchar u1 = 0;
uchar u2 = 0;

const int timer = 83;
int i = 0;

void delay(unit t);

void main(void)
{
	p2_0 = p2_1 = 0;
	
	while(1)
	{
		// 状态字的高8位
		u1 = (uchar)((state >> 8) & 0x00FF);
		// 状态字的低8位
		u2 = (uchar)(state & 0x00FF);
		
		// 显示第一排LED
		//p2_0 = (u1 < 256) ? 1 : 0;
		p2_0 = 1;
		P1 = u1;
		//delay(100);
		p2_0 = 0;
		//delay(100);
		
		// 显示第二排LED
		//p2_1 = (u2 < 256) ? 1 : 0;
		p2_1 = 1;
		P1 = u2;
		//delay(100);
		p2_1 = 0;
		
		delay(50);
		
		// 状态字的最高位
		temp = state >> 15;
		temp &= 0x0001;
		state <<= 1;
		state |= temp;
		
	}		
}

void delay(unit t)
{
	while(t--)
	{
		for(i = timer; i>=0; i--); //1毫秒
	}
}