// 四个灯循环滚动
#include <REGX52.H>

// 1毫秒延时函数
void delay(unsigned int count)
{
	unsigned int i,j;
	for(i=0;i<count;i++)
		for(j=0;j<120;j++);
}

void main(void)
{
	unsigned char s = 0x0f;
	unsigned char t = 0;
	
	while(1)
	{
		// 记录最低位的数值
		t = s & 0x01;
		// 将最低位左移7位，及移到最高位
		t <<= 7;
		// 清除除最高位之外的其它位上的数值
		t &= 0x80;
		
		// 灯状态右移一位
		s >>= 1;
		// 将移出的最低位补到最高位上
		s |= t;
		// 显示效果
		P1 = s;
		
		delay(100);
	}	
}