/****************************************
*@文件: DS18B20.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-24
*@描述: DS18B20温度感应IC实验
*       通过单片机采集DS18B20的温度变化，并将获得到的温度显示在数码管上
*@接线: P1口控制数码管的段显示，P0口的第四位控制数码管的位选择，P07采集DS18B20的温度
****************************************/

#include <REGX52.H>
#include <intrins.h>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

// 全局变量
// 八段码位选择
sbit S1 = P0^0;
sbit S2 = P0^1;
sbit S3 = P0^2;
sbit S4 = P0^3;
// DS18B20温度采集总线
//sbit DQ = P0^7;
// 最终温度, 符号位，整数位，小数位
//uchar temps[3];

// 数码管编码表0-9，-
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0xBF
};


// 要显示的内容
uchar screen[4] = {0x99, 0xF9, 0x90, 0xBF};


// 函数声明
void display(uchar c, uchar position);

/****************************************
*@描述: 主函数
****************************************/
void main(void)
{
	uchar ch;

	while(1)
	{		
		ch = 200;
		while(--ch)
		{
			// 符号位
			display(screen[3], 1);
			// 十位
			display(screen[2], 2);
			// 各位及小数点符号
			display(screen[1], 3);
			// 小数位
			display(screen[0], 4);
		}
	}
}

/****************************************
*@描述: 7段数码管显示
*@参数: unsigned char c 显示的字符
*@参数: unsigned char position 显示的位置
*@返回: void
****************************************/
void display(uchar c, uchar position)
{
	uint tm = 0;
	
	S1 = S2 = S3 = S4 = 1;
	
	for(tm = 0; tm < 10; tm++)
	{
		// 位选择
		switch(position)
		{
			case 1:
				S1 = 0;
				break;
			case 2:
				S2 = 0;
				break;
			case 3:
				S3 = 0;
				break;
			case 4:
				S4 = 0;
				break;
		}
		// 段显示
		P1 = c;
	}
}
