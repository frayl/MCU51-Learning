/****************************************
*@文件: DS18B20.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-24
*@描述: DS18B20温度感应IC实验
*       通过单片机采集DS18B20的温度变化，并将获得到的温度显示在数码管上
*@接线: P1口控制数码管的段显示，P0口的第四位控制数码管的位选择，P07采集DS18B20的温度
****************************************/

#include <REGX52.H>
#include <intrins.h>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

// 全局变量
// 八段码位选择
sbit S1 = P0^0;
sbit S2 = P0^1;
sbit S3 = P0^2;
sbit S4 = P0^3;
// DS18B20温度采集总线
sbit DQ = P0^7;
// 最终温度, 符号位，整数位，小数位
uchar temps[3];

// 数码管编码表0-9，-
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0xBF
};

// 将DS18B20的小数部分的0-f刻度转换为0-9刻度的查找表,将精度化为0.1度
uchar code fraction[]={0,0,1,2,2,3,4,4,5,6,6,7,8,8,9,9};

// 要显示的内容
uchar screen[4] = {0, 0, 0, 0};


// 函数声明
void display(uchar c, uchar position);
void delay(uint);
void resetDS(void);
uchar readDS(void);
void writeDS(uchar);
void readTemp(void);

/****************************************
*@描述: 主函数
****************************************/
void main(void)
{
	uchar ch;

	while(1)
	{
		// 采集温度
		readTemp();
		
		// 处理要显示的字符信息
		// 符号位
		if(temps[2] == 1)
			screen[3] = table[10];
		else
			screen[3] = 0xFF;
		// 温度十位
		ch = temps[1] / 10;
		screen[2] = table[ch];
			
		// 温度个位，及小数点符号
		ch = temps[1] % 10;
		ch = table[ch] & 0x7F;
		screen[1] = ch;
		
		// 小数位
		ch = temps[0];
		screen[0] = table[ch];
		
		// 显示采集的温度
		ch = 500;
		while(--ch)
		{
			// 符号位
			display(screen[3], 1);
			// 十位
			display(screen[2], 2);
			// 各位及小数点符号
			display(screen[1], 3);
			// 小数位
			display(screen[0], 4);
		}
	}
}

/****************************************
*@描述: 7段数码管显示
*@参数: unsigned char c 显示的字符
*@参数: unsigned char position 显示的位置
*@返回: void
****************************************/
void display(uchar c, uchar position)
{
	uint tm = 0;
	
	S1 = S2 = S3 = S4 = 1;
	
	for(tm = 0; tm < 10; tm++)
	{
		// 位选择
		switch(position)
		{
			case 1:
				S1 = 0;
				break;
			case 2:
				S2 = 0;
				break;
			case 3:
				S3 = 0;
				break;
			case 4:
				S4 = 0;
				break;
		}
		// 段显示
		P1 = c;
	}
}

/****************************************
*@描述: DS18B20复位函数
*@参数: void
*@返回: void
****************************************/
void resetDS(void)
{
	// 拉低,开始复位操作
	DQ = 0;
	// 延时至少480us
	delay(100);
	// 拉高，释放总线控制权
	DQ = 1;
	// 等待器件应答（器件拉低），约15-60us后
	while(DQ);
	// 应答脉冲出现后，等待器件拉高，约60-240us后
	while(!DQ);
}

/****************************************
*@描述: DS18B20读数据函数，注意读出的数据低位在前，高位在后
*@参数: void
*@返回: unsigned char
****************************************/
uchar readDS(void)
{
	uchar i, dat = 0;
	for(i = 0; i < 8; i++)
	{
		//dat <<= 1;
		// 开始读操作
		DQ = 0;
		// 延时必须大于1us
		_nop_();_nop_();
		//释放总线控制权，15us内要读取数据
		DQ = 1;
		if(DQ == 1) dat |= (0x01 << i);
		// 延时要大于45us.读0时，45us后器件才拉高总线
		delay(10);
	}
	
	return dat;
}

/****************************************
*@描述: DS18B20写命令函数
*@参数: unsigned char
*@返回: void
****************************************/
void writeDS(uchar cmd)
{
	uchar i = 0;
	for(i = 0; i < 8; i++)
	{
		// 开始写操作
		DQ = 0;
		// 延时必须在15us内
		_nop_();_nop_();
		DQ = cmd & 0x01;
		// 延时，器件在45us内采样
		delay(3);
		// 释放总线控制权
		DQ = 1;
		cmd >>= 1;
	}
}

/****************************************
*@描述: 读出温度函数
*@参数: void
*@返回: void
****************************************/
void readTemp(void)
{
	uchar dat[2], temp, n;
	
	// 复位
	resetDS();
	// 写命令，跳过ROM编码命令
	writeDS(0xCC);
	// DS18B20启动一次温度转换
	writeDS(0x44);
	// 等待转换完成
	while(!DQ);
	// 复位
	resetDS();
	// 写命令，跳过ROM编码命令
	writeDS(0xCC);
	// 读取DS18B20暂存器数据
	writeDS(0xBE);
	// 温度数据低字节
	dat[0] = readDS();
	// 温度数据高字节
	dat[1] = readDS();
	// 复位
	resetDS();
	// 温度，去掉小数部分
	temp = ((dat[1] << 4) & 0xF0) | ((dat[0] >> 4) & 0x0F);
	// 温度为负
	if((dat[1] & 0x80) == 0x80)
	{
		temps[2] = 1;
		temps[1] = ~temp + 1;
		n = (~dat[0] + 1) & 0x0F;
		temps[0] = fraction[n];
	}
	else
	{
		temps[2] = 0;
		temps[1] = temp;
		n = dat[0] & 0x0F;
		temps[0] = fraction[n];
	}
}

/****************************************
*@描述: 微秒延时函数
*       此函数精确计算：18+6*(t-1)=延时时间(us)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}
