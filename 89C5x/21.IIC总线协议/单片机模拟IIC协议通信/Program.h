// 说明：程序头文件定义

#ifndef __PROGRAM_H__

#define __PROGRAM_H__

// 向设备发送数据
// device：设备号，末尾必须为0
// addr：数据地址
// byte：发送的内容
void write_byte(uchar device, uchar addr, uchar byte);

// 从设备读取数据
// device：设备号，末尾必须为0
// addr：数据地址
uchar read_byte(uchar device, uchar addr);

// 向设备发送任意长数据
// device：设备号，末尾必须为0
// addr：数据地址
// str：发送的数据
// strlen：字符串长度
void write_data(uchar device, uchar addr, uchar *str, uint strlen);

// 从设备读取任意长数据
// device：设备号，末尾必须为0
// addr：数据地址
// str：接收到的数据
// strlen：要获得的数据长度
void read_data(uchar device, uchar addr, uchar *str, uint strlen);

#endif