// 描述：通用库头文件

#ifndef __COMMON_H__

#define __COMMON_H__

#define uint unsigned int
#define uchar unsigned char
	
#define TRUE 1
#define FALSE 0

// ASCII码转化为数码管编码(共阳极)
uchar asc2led(uchar);

// 毫秒延时函数
void delay(uint);

// 微秒延时函数
void wait(uchar);

#endif