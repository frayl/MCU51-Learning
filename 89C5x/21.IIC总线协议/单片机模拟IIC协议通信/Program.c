// 功能：单片机模拟IIC协议通信
// 平台：Proteus
// 实现：单片机模拟IIC通信协议，操作EEPROM(AT24C02)存取数据

// 引脚定义：
// SDA: P2.0
// SCL: P2.1

#include <REGX52.H>
//#include <STDLIB.H>
#include "Common.h"
#include "IIC.h"
#include "Program.h"

//#define WAIT_TIME 60

sbit SDA = P2^0;
sbit SCL = P2^1;


void main(void)
{
	uchar byte;
	uchar device, i;
	uchar *ss = "FEDCBA9876543210ABCDEFA9876543210A98765";
	uchar dd[39] = "                                       ";
	uchar *ptr = dd;
	
	P1 = 0xC0;
	
	// 设置要操作的设备号
	device = 0xA0;
	
	/********************
	*  字节存取操作测试  *
	*********************/
	// 循环写入字符
//	for(i = 0; i < 10; i++)
//	{
//		write_byte(device, i, 48 + i);
//	}
//	for(i = 0; i < 6; i++)
//	{
//		write_byte(device, 10 + i, 65 + i);
//	}
//	
//	// 读出写入的数据
//	for(i = 0; i < 16; i++)
//	{
//		byte = read_byte(device, i);
//		// 显示读出的内容
//		P1 = asc2led(byte);
//		
//		delay(2);
//	}

	/********************
	*  字串存取操作测试  *
	*********************/
	write_data(device, 0, ss, 39);
	
	// malloc在Keil下分配内存不成功！暂时没有找到解决办法
	//dd = (uchar *)malloc(16);
	read_data(device, 0, dd, 39);
	
	for(i = 0; i < 39; i++)
	{
		//byte = read_byte(device, i);
		//P1 = asc2led(byte);
		// 显示读出的内容
		P1 = asc2led(*ptr++);
		
		delay(2);
	}
	
	while(1);
}

// 向设备发送任意长数据
// device：设备号，末尾必须为0
// addr：数据地址
// str：发送的数据
// strlen：字符串长度，最大长度不能超过16个字符
void write_data(uchar device, uchar addr, uchar *str, uint strlen)
{
	uint num = 0, i = 0, j = 0;
	uchar *ptr = str;
	
	// 缓存大小是16个字符，数据太长必须截断
	num = strlen / 16 + 1;
	
	for(j = 0; j < num; j++)
	{
		// 开始
		iic_start();
		// 连接设备
		iic_write(device);
		while(!iic_ack());
		// 发送存储地址
		iic_write(addr + (j * 16));
		while(!iic_ack());
		
		for(i = 0; i < 16; i++)
		{
			// 判断字符是否全部输出完毕
			if((j * 16 + i) < strlen)
			{
				// 写数据
				iic_write(*ptr++);
				while(!iic_ack());
			}
		}
		
		// 结束
		iic_stop();
	}
}

// 从设备读取任意长数据
// device：设备号，末尾必须为0
// addr：数据地址
// str：接收到的数据
// strlen：要获得的数据长度
void read_data(uchar device, uchar addr, uchar *str, uint strlen)
{
	uint i = 0;
	uchar *ptr = str;
		
	// 开始
	iic_start();
	// 连接设备
	iic_write(device);
	while(!iic_ack());
	// 发送存储地址
	iic_write(addr);
	while(!iic_ack());
	
	// 发送读操作命令
	iic_start();
	iic_write((device | 1));
	while(!iic_ack());
	
	//for(i = 0; i < strlen; i++)
	while(strlen--)
	{
		*ptr++ = iic_read();

		iic_sendack(strlen);
	}
	
	// 结束
	iic_stop();
}

// 向设备发送数据
// device：设备号，末尾必须为0
// addr：数据地址
// byte：发送的内容
void write_byte(uchar device, uchar addr, uchar byte)
{	
	// 开始
	iic_start();
	// 连接设备
	iic_write((device | 0));
	while(!iic_ack());
	// 发送存储地址
	iic_write(addr);
	while(!iic_ack());
	// 写数据
	iic_write(byte);
	while(!iic_ack());
	// 结束
	iic_stop();
}

// 从设备读取数据
// device：设备号，末尾必须为0
// addr：数据地址
uchar read_byte(uchar device, uchar addr)
{
	uchar byte = 0;
	
	// 开始
	iic_start();
	// 连接设备
	iic_write(device);
	while(!iic_ack());
	// 发送存储地址
	iic_write(addr);
	while(!iic_ack());
	
	// 发送读操作命令
	iic_start();
	iic_write((device | 1));
	while(!iic_ack());
	// 写数据
	byte = iic_read();
	iic_ack();
	// 结束
	iic_stop();
	
	return byte;
}

#include "Common.c"
#include "IIC.c"
