// 描述：通用函数库

#include "Common.h"

// ASCII码转化为数码管编码(共阳极)
uchar asc2led(uchar ascii)
{
	switch(ascii)
	{
		case '0':
			return 0xC0;
		case '1':
			return 0xF9;
		case '2':
			return 0xA4;
		case '3':
			return 0xB0;
		case '4':
			return 0x99;
		case '5':
			return 0x92;
		case '6':
			return 0x82;
		case '7':
			return 0xF8;
		case '8':
			return 0x80;
		case '9':
			return 0x90;
		case 'a':
		case 'A':
			return 0x88;
		case 'b':
		case 'B':
			return 0x83;
		case 'c':
		case 'C':
			return 0xC6;
		case 'd':
		case 'D':
			return 0xA1;
		case 'e':
		case 'E':
			return 0x86;
		case 'f':
		case 'F':
			return 0x8E;
	}
	return 0xFF;
}

// 毫秒延时函数，最小1毫秒
void delay(uint t)
{
	uint tm = 0;
	while(t--)
	{
		tm = 114;
		while(tm--);
	}
}

// 微秒延时函数,最小1.1微秒
void wait(uchar t)
{
	while(t--);
}