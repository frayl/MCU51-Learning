// 描述：IIC总线协议函数定义头文件

#ifndef __IIC_51_H__

#define __IIC_51_H__

#ifndef WAIT_TIME
#define WAIT_TIME 10
#endif

//sbit SDA = P2^0;
//sbit SCL = P2^1;

// 起始信号
void iic_start();

// 终止信号
void iic_stop();

// 接受设备产生的应答信号
// 返回值：ACK:0->应答,1->非应答
// 每传送一个字节的数据（地址及命令）后，接受设备都会产生一个应答信号。
// SCL为高电平期间，接受设备将SDA拉低表示应答，否则为非应答
bit iic_ack(void);

// 给设备发送应答或非应答信号
void iic_sendack(bit);

// 读数据一个字节的数据
// 通过IIC总线读回的数据
// SCL为高电平期间数据才是稳定的，所以在SCL高的时候读取SDA上的数据
uchar iic_read();

// 写数据
// 参数:要发送的数据  
// IIC总线进行数据传输时，SCL为高电平期间SDA上的数据必须保持稳定
// 只有SCL为低电平期间SDA上的数据才允许变化
// 数据传送时，先传高位在传低位
void iic_write(uchar);

#endif