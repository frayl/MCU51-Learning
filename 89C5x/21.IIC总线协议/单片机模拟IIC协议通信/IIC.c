// 描述：IIC总线实现函数包
#include <REGX52.H>
#include "Common.h"
#include "IIC.h"

// 起始信号
void iic_start()
{
	SCL = 1;
	SDA = 1;
	wait(WAIT_TIME);
	SDA = 0;
	wait(WAIT_TIME);
	SCL = 0;
	wait(WAIT_TIME);
}

// 终止信号
void iic_stop()
{
	SCL = 0;
	SDA = 0;
	SCL = 1;
	wait(WAIT_TIME);
	SDA = 1;
	wait(WAIT_TIME);
	SCL = 0;
	wait(WAIT_TIME);
}

// 接受设备产生的应答信号
// 返回值：ACK:0->应答,1->非应答
// 每传送一个字节的数据（地址及命令）后，接受设备都会产生一个应答信号。
// SCL为高电平期间，接受设备将SDA拉低表示应答，否则为非应答
bit iic_ack(void)
{
	bit v = 0;
	uchar i = 0;
	
	SDA = 1;
	SCL = 1;
	wait(WAIT_TIME/2);
	v = SDA;
	//while((SDA == 1) && (i < 250))i++;
	wait(WAIT_TIME/2);
	// SCL为低电平时SDA上的数据才允许变化,为传送下一个字节做准备
	SCL = 0; 
	wait(WAIT_TIME);
	
	return (v == 1) ? 0 : 1;
}

// 给设备发送应答或非应答信号
void iic_sendack(bit ack)
{
	//准备
	SCL = 0;
    wait(1);

	//ACK
    if(ack)
    {
        SDA = 0;
    }
	//unACK
    else
    {
        SDA = 1;
    }
  
    wait(1);
    SCL = 1;
    wait(WAIT_TIME);
	//钳住scl,以便继续接收数据
    SCL = 0;
    wait(1);
}

// 读数据一个字节的数据
// 通过IIC总线读回的数据
// SCL为高电平期间数据才是稳定的，所以在SCL高的时候读取SDA上的数据
uchar iic_read()
{
	uchar byte = 0, n = 0;

	//准备读数据
	SCL = 0;
	wait(1);
	// 释放总线
	SDA = 1;
	wait(1);
	
	for(n = 0; n < 8; n++)
	{
		byte <<= 1;
		
		// 时钟线拉高为读数据做准备
		SCL = 1;
		wait(WAIT_TIME);
		byte |= SDA;
		wait(WAIT_TIME);
		SCL = 0;
		wait(WAIT_TIME);
	}
		
	return byte;
}

// 写数据
// 参数:要发送的数据  
// IIC总线进行数据传输时，SCL为高电平期间SDA上的数据必须保持稳定
// 只有SCL为低电平期间SDA上的数据才允许变化
// 数据传送时，先传高位在传低位
void iic_write(uchar byte)
{
	bit temp;
	uchar n = 0;
	// 拉低时钟线,准备给SDA写数据
	SCL = 0;
	
	for(n = 0; n < 8; n++)
	{
		// 数据从高位开始发送
		temp = (byte & 0x80) > 0 ? 1 : 0;	

		SDA = temp;
		SCL = 1;
		wait(WAIT_TIME);
		SCL = 0;
		wait(WAIT_TIME);
		
		byte <<= 1;
	}
	
	//释放总线,数据总线不用时要释放
	SDA = 1;
	wait(1);
}