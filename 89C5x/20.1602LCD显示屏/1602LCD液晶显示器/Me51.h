// 说明：自定义51单片机头文件

#ifndef ME51

#define ME51 1

#define uint unsigned int
#define uchar unsigned char

// 液晶初始化
void init();

// 读状态
uchar read_state();

// 写命令
void write_cmd(uchar);

// 读数据
uchar read_data(uchar);

// 写数据
void write_data(uchar, uchar);

// 写字符串
// uchar* str, 字符串指针
// uchar len, 字符串长度
// uchar addr, 写入地址的偏移量
// uchar delaytime, 写入地址的偏移量
void write_string(uchar* str, uchar len, uchar addr, uchar delaytime);

// 是否能读写
uchar can_rw();

// 当前数据地址
uchar current_addr();

// 等待读写允许
void wait_allow();

// ASCII码转化为数码管编码
uchar show_led(uchar);

// 声明延时函数
void delay(uint);


#endif