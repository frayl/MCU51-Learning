// 功能：驱动1602LCD液晶显示器
// 平台：51开发版
// 实现：单片机驱动1602LCD模组

#include <REGX52.H>
#include "Me51.h"

sbit RS = P3^5;
sbit RW = P3^6;
sbit E = P3^4;

// 数码管编码表
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};

void main()
{
	uchar dat = 0, n = 0;
	
	init();
	
//	write_data('A', 0);
//	
//	write_data('9', 2);
//	
//	write_data('C', 5);
//	
//	write_data('%', 0x43);
//	write_string("0123456789ABCDEF---", 16, 0, 0);
	write_cmd(0x07);
	write_string("Hello, World!", 13, 0x10, 200);
//	delay(1000);
	write_cmd(0x06);
	write_string("I Like MCU!", 11, 0x50, 200);
	
//	P1 = 0xC0;
//	for(n = 0; n < 16; n++)
//	{
//		delay(1000);
//		P1 = ~read_data(n);
//	}
	write_data('9', 1);
	dat = read_data(1);
	P1 = show_led(dat);
	
	delay(5000);
	dat = current_addr();
	P1 = table[dat];
//	P1 = 0xC6;
	
	while(1);
}

// 液晶初始化
void init()
{
	// 显示模式设置
	write_cmd(0x38);
	// 开显示，显示光标，光标闪烁
	write_cmd(0x0F);
	// 设置写一个字符，整屏左移
	//write_cmd(0x07);
	// 清屏
	write_cmd(0x01);
}

// 读状态
uchar read_state()
{
	uchar tmp;
	
	P0 = 0xFF;
	
	RS = 0;
	RW = 1;
	E = 1;	
	delay(1);
	tmp = P0;
	E = 0;
	
	return tmp;
}

// 写命令
void write_cmd(uchar cmd)
{
	wait_allow();
	
	RS = 0;
	RW = 0;
	delay(1);
	P0 = cmd;
	delay(1);
	E = 1;
	delay(2);
	E = 0;
}

// 读数据
// uchar addr, 字符所在地址的偏移值，如：3
uchar read_data(uchar addr)
{
	uchar dat = 0;
	
	// 设置地址
	write_cmd(0x80 + addr);
	
	wait_allow();
		
//	RS = 0;
//	RW = 0;
//	E = 0;
//	delay(1);
	P0 = 0xFF;
	RS = 1;	
	RW = 1;
	delay(1);
	E = 1;
	delay(1);
	dat = P0;
	E = 0;
//	RW = 0;
//	RS = 0;
	
	return dat;
}

// 写数据
// uchar dat, 要写入的字符
// uchar addr, 字符写入的地址的偏移值，如：3
void write_data(uchar dat, uchar addr)
{
	// 设置写入地址
	write_cmd(0x80 + addr);
	
	wait_allow();
	
	RS = 1;
	RW = 0;
	delay(1);
	P0 = dat;
	delay(1);
	E = 1;
	delay(2);
	E = 0;
}

// 写字符串
void write_string(uchar* str, uchar strlen, uchar addr, uchar time)
{
	uchar i = 0;
	for(i = 0; i < strlen; i++)
	{
		delay(time);
		write_data(str[i], addr+i);
	}
}

// 是否能读写
// 读状态时返回一个Byte的状态字
// 最高位为读写使能操作，1：禁止，0：允许
// 返回：1：允许，0：不允许
uchar can_rw()
{
	uchar state = 0, temp = 0;
	
	state = read_state();
	
	temp = state & 0x80;
	temp >>= 7;
	
	return ~temp;
}

// 当前数据地址
// 读状态时返回一个Byte的状态字
// 低7位为当前数据地址指针
uchar current_addr()
{
	uchar state = 0;
	state = read_state();
	
	return (state & 0x7F);
}

// 等待读写允许
void wait_allow()
{
	uchar allow = 0;
	do
	{
		allow = can_rw();
	} 
	while(!allow);
}

// ASCII码转化为数码管编码
uchar show_led(uchar ascii)
{
	switch(ascii)
	{
		case '0':
			return 0xC0;
		case '1':
			return 0xF9;
		case '2':
			return 0xA4;
		case '3':
			return 0xB0;
		case '4':
			return 0x99;
		case '5':
			return 0x92;
		case '6':
			return 0x82;
		case '7':
			return 0xF8;
		case '8':
			return 0x80;
		case '9':
			return 0x90;
		case 'a':
		case 'A':
			return 0x88;
		case 'b':
		case 'B':
			return 0x83;
		case 'c':
		case 'C':
			return 0xC6;
		case 'd':
		case 'D':
			return 0xA1;
		case 'e':
		case 'E':
			return 0x86;
		case 'f':
		case 'F':
			return 0x8E;
	}
	return 0xFF;
}

// 延时函数
// 最小延时1ms
void delay(uint t)
{
	uchar tm = 0;
	while(t--)
	{
		tm = 10;
		while(tm--);
	}
}
