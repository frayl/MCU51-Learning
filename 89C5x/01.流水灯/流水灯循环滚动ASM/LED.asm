;**********************************************
;*@File: LED.asm
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-11
;*@Description: P1口控制8个LED灯循环滚动
;**********************************************
	ORG 0000H
	LJMP MAIN
	ORG 0100H
MAIN:
	;设置LED灯初始状态
	MOV A, #0FEH
	;循环滚动
LOOP:
	MOV P1, A		
	RL A
	;外部延时时长
	MOV R7, #0FFH
DELAY:
	;内部延时时长
	MOV R6, #0FFH
WAIT:
	DJNZ R6, WAIT
	DJNZ R7, DELAY
	SJMP LOOP
	END
