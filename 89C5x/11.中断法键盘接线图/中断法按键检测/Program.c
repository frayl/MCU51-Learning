// 11.中断发键盘接线图(Proteus) 演示程序
// 中断发键盘按键检测程序
// 使用了一个共阳极数码管，AT89C52单片机，4082与门
// 通过对列的按键信号进行与后，触发中断，进而检测按键

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
	
// 数码管编码0-F
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};
// 按键初识信号
uchar code sign[] = {0xEF, 0xDF, 0xBF, 0x7F};
// 以毫秒执行的指令数
uchar time = 114;
// 行值
uint row = 0;
// 列值
uint col = 0;
// 普通变量
uchar n = 0;
// 按键位置
uint scan = 0;

// 声明演示函数
void delay(uint t);

void main(void)
{
	// 设置外部中断0触发方式为低电平触发
	IT0 = 0;
	// 开外部中断
	EX0 = 1;
	// 开总中断
	EA = 1;
	// 行输出信号，列接收信号
	P2 = 0x0F; 
	// 
	while(1);
}


// 中断0执行程序
void int0() interrupt 0
{
	// 关闭中断，防止重复中断
	EA = 0;
	
	// 行输出信号，列接收信号
	//P2 = 0x0F;
	/*
	// 扫描法
	if((P2 & 0x0F) != 0x0F)
	{
		// 延时5毫秒，消除按键抖动
		delay(5);
		if((P2 & 0x0F) != 0x0F)
		{
			// 列位置
			col = P2 & 0x0F;
			for(n = 0; n < 4; n++)
			{
				P2 = sign[n];
				if((P2 & 0x0F) != 0x0F)
				{
					// 行位置
					row = sign[n] & 0xF0;
				}
			}
		}
	}*/
	
	// 线反转法
	P2 = 0x0F;
	if(P2 != 0x0F)
	{
		// 延时5毫秒，消除按键抖动
		delay(5);
		if(P2 != 0x0F)
		{
			// 列位置
			col = P2 & 0x0F;
			P2 = 0xF0;
			if(P2 != 0xF0)
			{
				// 行位置
				row = P2 & 0xF0;
			}
		}
	}
	
	// 松手检测
	P2 = 0x0F;
	while(P2 != 0x0F);
	
	switch(row)
	{
		case 0xE0:
			scan = 0;
			break;
		case 0xD0:
			scan = 1;
			break;
		case 0xB0:
			scan = 2;
			break;
		case 0x70:
			scan = 3;
			break;
	}
	
	switch(col)
	{
		case 0x0E:
			scan = (scan * 4);
			break;
		case 0x0D:
			scan = (scan * 4) + 1;
			break;
		case 0x0B:
			scan = (scan * 4) + 2;
			break;
		case 0x07:
			scan = (scan * 4) + 3;
			break;
	}
	
	
	
	// 数码管输出
	P1 = table[scan];
	// 重置输出信号，等待检测
	P2 = 0x0F;
	
	EA = 1;
}

// 延时函数，最小演示1毫秒
void delay(uint t)
{	
	while(t--)
	{
		time = 114;
		while(time--);
	}
}