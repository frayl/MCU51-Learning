// 功能：单机自发自收
// 平台：Proteus
// 实现：单片机通过串口发送一个字符，并显示在数码管上，然后又通过串口将接收到的字符显示在另一个数码管上
//       波特率9600，晶振频率11.0592MHz，开启波特率倍增
//       扫描法和中断法都测试成功

#include <REGX52.H>
#define uint unsigned int
#define uchar unsigned char

// 共阳极7段数码管0-F数字编码
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};

// 声明延时函数
void delay(uint t);

void main(void)
{
	uchar temp = 0;
	uchar n = 0;
	
	// 设置串口工作方式
	SCON = 0x50;
	// 波特率倍增
	//PCON = 0x80;    //SMOD = 1;
	// 设置定时器1工作方式
	TMOD = 0x20;
	// 设置定时寄存器初值
	TH1 = TL1 = 0xFA;
	// 启动定时器1
	TR1 = 1;
	// 打开串口
	ES = 1;
	// 打开中断
	EA = 1;
	
//	// 扫描法
//	while(1)
//	{
//		// 数码管不显示信息
//		P1 = P2 = table[16];
//		
//		if(temp++ == 255)
//		{
//			temp = 0;
//		}
//		// 得到一个0-16之间的数
//		n = temp % 16;
//		
//		// 发送信息
//		SBUF = table[n];
//		while(!TI);
//		TI = 0;
//		// 将字符显示在数码管上
//		P1 = table[n];

//		delay(5);
//		
//		// 接收信息
//		while(!RI);
//		RI = 0;
//		P2 = SBUF;
//		
//		delay(5);
//	}
	
	// 中断法
	while(1)
	{
		// 数码管不显示信息
		P1 = P2 = table[16];
		
		// 数码管不显示信息
		P1 = P2 = table[16];
		
		if(temp++ == 255)
		{
			temp = 0;
		}
		// 得到一个0-16之间的数
		n = temp % 16;
		
		// 发送信息
		SBUF = table[n];
		// 将字符显示在数码管上
		P1 = table[n];

		delay(5);
	}
}

// 串口中断函数
void Serial() interrupt 4
{
	// 接收到信息，并在数码管上显示
	if(RI == 1)
	{
		P2 = SBUF;
		RI = 0;
	}
	
	// 发送完毕
	if(TI == 1)
	{
		TI = 0;
	}
}

// 延时函数
void delay(uint t)
{
	uint tm = 0;
	while(t--)
	{
		tm = 114;
		while(tm--);
	}
}
