// 功能：串口通信实验
// 平台：Proteus
// 实现：通过串口中断方法，实现单片机收发信息的功能

#include <REGX52.H>

unsigned char dat = 0;

void main(void)
{
	// 设置定时器1的工作方式
	TMOD |= 0x20;
	// 设置串口的工作方式
	SCON = 0x50;
	// 设置串口波特率倍增
	//PCON = 0x80;
	// 设置定时器1的定时计数器初值
	TH1 = TL1 = 0xFD;
	// 打开定时器
	TR1 = 1;
	// 打开串口
	ES = 1;
	// 开中断
	EA = 1;
	
	while(1);
}

void Serial() interrupt 4 //using 0
{
	// 串口收到信息
	if(RI == 1)
	{
		// 取出接收到的数据
		dat = SBUF;
		// 清除接收中断标志位
		RI = 0;
		// 发送数据
		SBUF = dat;
		// 等待串口发送信息完毕
		while(!TI);
		// 清除串口放送中断标志位
		TI = 0;
	}
	
	// 判断是否发送信息完毕
//	if(TI == 1)
//	{
//		TI = 0;
//	}
}
