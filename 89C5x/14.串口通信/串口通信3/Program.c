// 功能：串口通信实验
// 平台：Proteus
// 实现：通过串口中断方法，实现单片机收发信息的功能
// 技术：可自定义晶振平率和串口传输所使用的波特率

#include <REGX52.H>

unsigned char dat = 0;

// 设置晶振的频率
unsigned long FOSC = 11059200L;
// 设置波特率
//unsigned long BAUD = 9600L;
unsigned long BAUD = 19200L;
// 定时器初值, 初值最好在确定了波特率和晶振频率后直接算出来，让单片机技算很占程序空间
unsigned char thl = 0; 

void main(void)
{
	// 设置定时器1的工作方式
	TMOD |= 0x20;
	// 设置串口的工作方式
	SCON = 0x50;
	// 设置串口波特率倍增
	PCON = 0x80;
	// 设置定时器1的定时计数器初值, 开启了波特率倍增，若传输使用的波特率为9600，则除值=0xFA
	//TH1 = TL1 = 0xFA;
	thl = 256 - (2 * FOSC) / (384 * BAUD);
	TH1 = thl;
	TL1 = thl;
	// 打开定时器
	TR1 = 1;
	// 打开串口
	ES = 1;
	// 开中断
	EA = 1;
	
	while(1);
}

void Serial() interrupt 4 //using 0
{
	// 串口收到信息
	if(RI == 1)
	{
		// 取出接收到的数据
		dat = SBUF;
		// 清除接收中断标志位
		RI = 0;
		// 发送数据
		SBUF = dat;
		// 等待串口发送信息完毕
		//while(!TI);
		// 清除串口放送中断标志位
		//TI = 0;
	}
	
	// 判断是否发送信息完毕
	if(TI == 1)
	{
		TI = 0;
	}
}
