// 功能：串口通信实验
// 平台：Proteus
// 实现：扫描串口接收标志位，收到信息后立即发送出去

#include <REGX52.H>

void main(void)
{
	unsigned char temp;
	
	// 设置定时器工作方式
	TMOD = 0x20;
	// 设置串口工作方式
	SCON = 0x50;
	// 设置定时计数器初值
	TH1 = 0xFD;
	TL1 = 0xFD;
	// 启动定时器
	TR1 = 1;
	
	while(1)
	{
		if(RI == 1)
		{	
			temp = SBUF;
			RI = 0;
			SBUF = temp;
			// 检测串口是否发生完毕
			while(!TI);
			TI = 0;
		}
	}
}