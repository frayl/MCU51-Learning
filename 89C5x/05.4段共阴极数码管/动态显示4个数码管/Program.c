// Protues仿真
// 动态显示4个数码管,共阴极模式

#include <REGX52.H>

#define uchar unsigned char
	
sbit S1 = P2^0;
sbit S2 = P2^1;
sbit S3 = P2^2;
sbit S4 = P2^3;
uchar timer;


uchar value[] = {0, 0, 0, 0};

uchar code table[] = {
	0x3f,0x06,0x5b,0x4f,
	0x66,0x6d,0x7d,0x07,
	0x7f,0x6f,0x77,0x7c,
	0x39,0x5e,0x79,0x71
};

// 声明函数
void delay(uchar t);

void main(void)
{
	// 初始化数码管
	//S1 = S2 = S3 = S4 = 1;
	//P1 = table[0];
	//S1 = S2 = S3 = S4 = 0;
	
	while(1)
	{
		// 第1段数码管		
		value[0] = 0;
		while(value[0] < 16)
		{
			S1 = 1;
			P1 = table[value[0]];
			S1 = 0;
			value[0]++;
			
			// 第2段数码管
			value[1] = 0;
			while(value[1] < 16)
			{
				S2 = 1;
				P1 = table[value[1]];
				S2 = 0;
				value[1]++;
				
				// 第3段数码管
				value[2] = 0;
				while(value[2] < 16)
				{
					S3 = 1;
					P1 = table[value[2]];
					S3 = 0;
					value[2]++;
					
					value[3] = 0;
					while(value[3] < 16)
					{						
						S4 = 1;
						P1 = table[value[3]];
						S4 = 0;
						
						value[3]++;
						
						delay(30);
					}
				}
			}
		}
	}
}

// 延时x毫秒
void delay(uchar t)
{
	while(t--)
	{
		for(timer = 84; timer > 0; timer--);
	}
}
