// 功能：智能蓄水池
// 平台：Proteus
// 器材：AT89C52,开关（模拟缺水、水满），	LED，NPN三极管，继电器，等
// 实现：扫描IO口，实现蓄水池自动抽水和停止抽水
//       缺水（SW1断开，红色指示灯亮，启动继电器工作）
//       水满（SW2闭合，绿色指示灯亮，抽水停止）

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char

sbit Work = P1^2;
sbit RED = P1^3;
sbit GREEN = P1^4;

void delay(unsigned int t);

void main(void)
{
	while(1)
	{
		// P1.0和P1.1用于输入
		P1 |= 0x03;
		
		// 判断SW1开关是否打开了
		if((P1 & 0x01) == 0x01)
		{
			delay(5);
			if((P1 & 0x01) == 0x01)
			{
				// 启动继电器工作
				Work = 0;
				// 变化指示灯
				RED = 0;
				GREEN = 1;
			}
		}
		
		// 判断SW2开关是否关闭了
		if((P1 & 0x02) == 0)
		{
			delay(5);
			if((P1 & 0x02) == 0)
			{
				// 关闭继电器
				Work = 1;
				// 变化指示灯
				RED = 1;
				GREEN = 0;
			}
		}
		
		delay(500);
	}
}

void delay(unsigned int t)
{
	unsigned char s = 0;
	while(t--)
	{
		s = 114;
		while(s--);
	}
}
