// 功能：智能蓄水池
// 平台：Proteus
// 器材：AT89C52,开关（模拟缺水、水满），LED，与非门，三极管，继电器，等
// 实现：缺水（SW1断开，继电器工作、开始抽水，红色LED亮）
//       水满（SW2闭合，触发外部中断0，继电器停止工作，抽水停止，绿色LED亮）

#include <REGX52.H>

#define uint unsigned int

sbit Work = P1^0;
sbit RED = P1^1;
sbit GREEN = P1^2;

// 声明延时函数
void delay(uint t);

void main(void)
{
	// 设置外部中断0工作方式为低电平触发
	IT0 = 0;
	// 开外部中断0
	EX0 = 1;
	// 打开中断开关
	EA = 1;
	
	// 初始化指示灯
	GREEN = 0;
	
	// 扫描P1.7,检测开关SW1是否断开了
	while(1)
	{
		P1 |= 0x80;
		// 判断SW1是否断开了
		if((P1 & 0x80) == 0x80)
		{
			delay(5);
			// SW1确实断开了
			if((P1 & 0x80) == 0x80)
			{
				// 启动继电器
				Work = 0;
				// 切换指示灯
				RED = 0;
				GREEN = 1;
				// 打开外部中断
				EX0 = 1;
			}
		}
	}
}

// 外部中断0函数
void int0() interrupt 0
{
	// 防止重复触发中断
	EA = 0;	
	// 继电器停止工作
	Work = 1;
	// 变换指示灯
	RED = 1;
	GREEN = 0;
	// 关闭外部中断，防止水满时反复触发中断
	EX0 = 0;
	EA = 1;
}

// 延时函数, 可以用定时器中断方法代替
void delay(uint t)
{
	uint tm = 0;
	while(t--)
	{
		tm = 114;
		while(tm--);
	}
}
