/****************************************
*@文件: 4-5Driven.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-24
*@描述: 查表法驱动4相5线步进电机【51开发板】
*       如果电机转动没有噪声，则表明转速正常
*@接线: P10-P13接ULN2003的MOT1-MOT4, P00-P03口控制四个按键
****************************************/

#include <REGX52.H>

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;
// 相位数目
#define STEP 8
// 每次按键速度的变化量
#define SEED 20
// 步进电机转动方向
#define FORWORD 1
#define BACK 0

// 单4拍相位表(A-B-C-D)
char code table1[4] = {0x0E, 0x0D, 0x0B, 0x07};
// 双4拍相位表(AB-BC-CD-DA)
char code table2[4] = {0x0C, 0x09, 0x03, 0x06};
// 单8拍相位表(A-AB-B-BC-C-CD-D-DA)
char code table3[8] = {0x0E, 0x0C, 0x0D, 0x09, 0x0B, 0x03, 0x07, 0x06};

uchar flag = 0;
// 步进电机转动方向
uchar direct = FORWORD;
// 步进电机转动速度
uint speed = 400;


void delay(uint);
void scanKey(void);

void main(void)
{
	// P0口做输入
	P0 = 0x0F;
	while(1)
	{
		// 按键检测
		scanKey();
		if(direct == FORWORD)
		{
			P2 = 0xFE;
		} else {
			P2 = 0xFF;
		}
		// 步进电机控制
		P1 = table3[flag];
		// 此处可调节转速，但似乎数值在170～200之间转速比较正常
		delay(speed);
		
		// 正转控制
		if(direct == FORWORD)
		{
			if(++flag == STEP)
				flag = 0;
		}
		// 反转控制
		else if(direct == BACK)
		{
			if(--flag == -1)
				flag = STEP-1;
		}
	}
}

/****************************************
*@描述: 按键扫描
*@参数: void
*@返回: void
****************************************/
void scanKey(void)
{
	// 是否按了UP键
	if((P0 & 0x0F) == 0x0E)
	{
		// 按键消抖
		delay(5000);
		if((P0 & 0x0F) == 0x0E)
		{
			speed += SEED;
		}
	}
	
	// 是否按了DOWN键
	if((P0 & 0x0F) == 0x0D)
	{
		// 按键消抖
		delay(5000);
		if((P0 & 0x0F) == 0x0D)
		{
			speed -= SEED;
		}
	}
	
	// 是否按了FORWORD键
	if((P0 & 0x0F) == 0x0B)
	{
		// 按键消抖
		delay(5000);
		if((P0 & 0x0F) == 0x0B)
		{
			direct = FORWORD;
		}
	}
	
	// 是否按了BACK键
	if((P0 & 0x0F) == 0x07)
	{
		// 按键消抖
		delay(5000);
		if((P0 & 0x0F) == 0x07)
		{
			direct = BACK;
		}
	}
}

/****************************************
*@描述: 微秒级延时函数
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}
