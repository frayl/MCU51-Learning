/****************************************
*@文件: 4-5Driven.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-24
*@描述: 空语句延时法驱动4相5线步进电机【51开发板】
*       如果电机转动没有噪声，则表明转速正常
*@接线: P10-P13接ULN2003的MOT1-MOT4
****************************************/

#include <REGX52.H>

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;
	
// 步进电机相位数控制
#define STEP 4
// 步进电机4个相位
sbit MA = P1^0;
sbit MB = P1^1;
sbit MC = P1^2;
sbit MD = P1^3;

uchar times = 0;
uchar flag = 0;

void delay(uint);
void step4();

void main(void)
{
	MA = MB = MC = MD = 1;
	
	while(1)
	{
		step4();
		// 此处可调节转速，但似乎数值在170～200之间转速比较正常
		delay(200);
		// 正转控制
		//if(++flag == 4) flag = 0;
		// 反转控制
		if(--flag == -1) flag = 3;
	}
}

/****************************************
*@描述: 4相4拍(AB-BC-CD-DA-AB)
*@参数: void
*@返回: void
****************************************/
void step4(void)
{
	switch(flag)
	{
		// AB
		case 0:
			MA = 0;
			MB = 0;
			MC = 1;
			MD = 1;
			//flag = 1;
			break;
		// BC
		case 1:
			MA = 1;
			MB = 0;
			MC = 0;
			MD = 1;
			//flag = 2;
			break;
		// CD
		case 2:
			MA = 1;
			MB = 1;
			MC = 0;
			MD = 0;
			//flag = 3;
			break;
		// DA
		case 3:
			MA = 0;
			MB = 1;
			MC = 1;
			MD = 0;
			//flag = 0;
			break;
	}
}

/****************************************
*@描述: 微秒级延时函数
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(t--);
}
