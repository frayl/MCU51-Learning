/****************************************
*@文件: 4-5Driven.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-1-24
*@描述: 中断延时法驱动4相5线步进电机【51开发板】
*       晶振频率11.0592MHz
*@接线: P10-P13接ULN2003的MOT1-MOT4
****************************************/

#include <REGX52.H>

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;
	
// 步进电机相位数控制
#define STEP 4
// 步进电机4个相位
sbit MA = P1^0;
sbit MB = P1^1;
sbit MC = P1^2;
sbit MD = P1^3;

uchar times = 0;
uchar flag = 0;

//void delay(uint);
void step4();

void main(void)
{
	//初始化定时器T0, 最小定时1ms
	TMOD = 0x01;
	TH0 = 0xDB;
	TL0 = 0xFF;
	ET0 = 1;
	TR0 = 1;
	EA = 1;
	
	MA = MB = MC = MD = 1;
	
	while(1)
	{
		step4();
	}
}

/****************************************
*@描述: 定时器T0定时中断函数，最低定时1ms
*@参数: void
*@返回: void
****************************************/
void timer0() interrupt 1 using 1
{
	TH0 = 0xDB;
	TL0 = 0xFF;
	
	times++;
	if(times == STEP)
	{
		times = 0;
		flag++;
		if(flag == 4)  flag = 0;
	}
}

/****************************************
*@描述: 4相4拍(AB-BC-CD-DA-AB)
*@参数: void
*@返回: void
****************************************/
void step4(void)
{
	switch(flag)
	{
		// AB
		case 0:
			MA = 0;
			MB = 0;
			MC = 1;
			MD = 1;
			//flag = 1;
			break;
		// BC
		case 1:
			MA = 1;
			MB = 0;
			MC = 0;
			MD = 1;
			//flag = 2;
			break;
		// CD
		case 2:
			MA = 1;
			MB = 1;
			MC = 0;
			MD = 0;
			//flag = 3;
			break;
		// DA
		case 3:
			MA = 0;
			MB = 1;
			MC = 1;
			MD = 0;
			//flag = 0;
			break;
	}
}

/****************************************
*@描述: 延时函数，最低延时10ms
*@参数: unsigned int
*@返回: void
****************************************/
//void delay(uint t)
//{
//	uchar t = 0;
//	while(t--)
//	{
//		for(t = 114; t > 0; t--);
//	}
//}