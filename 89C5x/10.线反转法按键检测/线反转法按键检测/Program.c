// 非编码键盘按键检测
// 学习按钮检测，处理按键抖动问题，并将检测到的按键显示在数码管上
// 行上带上拉电阻

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
	
uchar code line[] = {
	0xEF, 0xDF, 0xBF, 0x7F, // 列
	0xFE, 0xFD, 0xFB, 0xF7  // 行
};

// 数码管编码0-F
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};

// 每毫秒执行的指令数
uchar seconds = 0;
uchar scan = 16;
//uchar temp = 0;
uchar key = 0;
uchar col = 0;
uchar row = 0;

// 声明按键扫描函数
uchar keyscan();
// 声明延时函数
void delay(uint t);

///////////////////////
void main(void)
{
	P2 = 0x01;
	while(1)
	{		
		key = keyscan();		
		P0 = table[key];
		delay(5);
	}
}

// 按键扫描
uchar keyscan()
{	
	// 给列输出低电平，行输出高电平
	P1 = 0x0F;
	// 检测是否有键按下
	if(P1 != 0x0F)
	{
		// 确定列的位置
		switch(P1)
		{
			case 0x07:
				col = 3;
				break;
			case 0x0B:
				col = 2;
				break;
			case 0x0D:
				col = 1;
				break;
			case 0x0E:
				col = 0;
				break;
		}
		
		P1 = 0xF0;
		delay(10);
		// 确定行的位置
		switch(P1)
		{
			case 0x70:
				row = 3;
				break;
			case 0xB0:
				row = 2;
				break;
			case 0xD0:
				row = 1;
				break;
			case 0xE0:
				row = 0;
				break;
		}
		scan = row * 4 + col;
	}
	
	return scan;
}

// 延时函数
void delay(uint t)
{	
	while(t--)
	{
		seconds = 114;
		while(seconds--);
	}
}
