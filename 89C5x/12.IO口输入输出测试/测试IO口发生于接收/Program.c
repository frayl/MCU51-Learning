// Proteus(12.IO口输入输出测试)
// 测试IO口发送信息和接收信息的状态变化
// 材料：LED，双掷开关
// 给P2口赋值一个固定信号，以接收信息，然后通过开关改变输入信号，看IO口信号的变化

#include <REGX52.H>

void main(void)
{
	P2 = 0xF0;
	while(1) {
		P1 = P2;
	}
}