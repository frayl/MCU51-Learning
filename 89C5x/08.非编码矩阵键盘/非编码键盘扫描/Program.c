// 非编码键盘按键检测
// 学习按钮检测，处理按键抖动问题，并将检测到的按键显示在数码管上
// 行上不带上拉电阻

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
	
uchar code line[] = {
	0xFE, 0xFD, 0xFB, 0xF7, // 列
	0xEF, 0xDF, 0xBF, 0x7F  // 行
};

// 数码管编码0-F
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};

// 每毫秒执行的指令数
uchar seconds = 0;
uchar scan = 0;
uchar temp = 0;
uchar key = 0;
uchar col = 0;
uchar row = 0;

// 声明按键扫描函数
uchar keyscan();
// 声明延时函数
void delay(uint t);

///////////////////////
void main(void)
{
	P2 = 0x01;
	while(1)
	{		
		key = keyscan();		
		P0 = table[key];
		delay(5);
	}
}

// 按键扫描
uchar keyscan()
{	
	// 给列输出低电平，行输出高电平
	P1 = 0xF0;
	// 检测是否有键按下
	if((P1 & 0xF0) != 0xF0)
	{
		// 延时5ms，消除按键抖动
		delay(5);
		// 消除抖动后，确实有按键按下
		if((P1 & 0xF0) != 0xF0)
		{
			// 逐列改变输出信号
			for(col = 0; col < 4; col++)
			{
				P1 = line[col];
				if((P1 & 0xF0) != 0xF0)
				{
					// 逐行扫描，确定按键位置
					for(row = 0; row < 4; row++)
					{				
						// 有键按下
						temp = line[row+4] & 0xF0;
						if((P1 & temp) == temp)
						{
							scan = line[row+4] & line[col];
						}
					}
				}
			}
		}
	}
	
	//获得按键按钮序号
	switch(scan)
	{
		case 0xEE:
			return 0;
			break;
		case 0xED:
			return 1;
			break;
		case 0xEB:
			return 2;
			break;
		case 0xE7:
			return 3;
			break;
		case 0xDE:
			return 4;
			break;
		case 0xDD:
			return 5;
			break;
		case 0xDB:
			return 6;
			break;
		case 0xD7:
			return 7;
			break;
		case 0xBE:
			return 8;
			break;
		case 0xBD:
			return 9;
			break;
		case 0xBB:
			return 10;
			break;
		case 0xB7:
			return 11;
			break;
		case 0x7E:
			return 12;
			break;
		case 0x7D:
			return 13;
			break;
		case 0x7B:
			return 14;
			break;
		case 0x77:
			return 15;
			break;
		default:
			return 16;
			break;
	}
}

// 延时函数
void delay(uint t)
{	
	while(t--)
	{
		seconds = 114;
		while(seconds--);
	}
}
