// 功能：多机通信（主机）
// 平台：Proteus
// 实现：通过串口实现多台单片机之间的相互通信
//       默认波特率为9600，晶振频率为11.0592MHz，开启电源的波特率倍增

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
#define Machine1 0x01
#define Machine2 0x02

// 当前通信主机
uchar current = 0;
uchar senddata = 0;
// 当前通信内容
uchar info = 0;
// 共阳极7段数码管0-F数字编码
uchar code table[] = {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E,
	0xFF
};
// 声明延时函数
void delay(uint t);

void main(void)
{
	uchar temp = 0;
	
	// 设置串口通信方式为：方式3，允许接收，TB8 = 1;
	SCON = 0xD8;
	// 波特率倍增
	PCON = 0x80;
	// 设置定时器1的工作方式为：方式2，8为自动重装
	TMOD = 0x20;
	// 设置定时器1的初值
	TH1 = TL1 = 0xFA;
	// 启动定时器1
	TR1 =1;
	// 打开串口
	ES = 1;
	// 开启中断
	EA = 1;
	
	while(1)
	{
		++current;
		if(current == 256)
		{
			current = 1;
		}
		
		temp = (current % 2) + 1;
		
		// 地址帧
		TB8 = 1;
		// 连接从机1
		if(temp == 1)
		{			
			SBUF = Machine1;
			// 显示当前通信的从机号
			P1 = table[1];
			P2 = table[16];
		} 
		// 连接从机2
		else if(temp == 2)
		{
			SBUF = Machine2;
			// 显示当前通信的从机号
			P1 = table[2];
			P2 = table[16];
		}
		
		// 延时2秒
		delay(200);
	}
}

// 串口中断函数
void Serial() interrupt 4
{
	uchar tmp = 0;
	
	// 是否收到信息
	if(RI == 1)
	{
		info = SBUF;
		RI = 0;
		
		// 获得一个3-15之间的数字
		++senddata;
		if(senddata == 256)
		{
			senddata = 0;
		}
		//tmp = (senddata % 13) + 3;
		tmp = senddata % 16;
			
		// 接收到的是从机请求信息
		if((info == Machine1) || (info == Machine2))
		{			
			// 数据帧
			TB8 = 0;
			// 发送数据
			SBUF = table[tmp];
			// 同时将发送的信息显示到数码管上
			P2 = table[tmp];
		}
		// 接收到的是从机发送的信息
		//else
		//{
			// 将信息显示到数码管上
		//	P2 = info;
		//}
	}
	// 信息发送完毕，清除发送中断标志位
	if(TI == 1)
	{
		TI = 0;
	}
}

// 延时函数，最小延时10ms
void delay(uint t)
{
	uchar tm = 0;
	while(t--)
	{
		tm = 114;
		while(tm--);
	}
}
