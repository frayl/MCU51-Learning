// 功能：多机通信（从机1）
// 平台：Proteus
// 实现：通过串口实现多台单片机之间的相互通信
//       默认波特率为9600，晶振频率为11.0592MHz，开启电源的波特率倍增

#include <REGX52.H>

#define uint unsigned int
#define uchar unsigned char
#define Machine 0x01

// 当前通信内容
uchar info = 0;

void main(void)
{	
	// 设置串口通信方式为：方式3，允许接收，SM2 = 1;
	SCON = 0xF0;
	// 波特率倍增
	PCON = 0x80;
	// 设置定时器1的工作方式为：方式2，8为自动重装
	TMOD = 0x20;
	// 设置定时器1的初值
	TH1 = TL1 = 0xFA;
	// 启动定时器1
	TR1 =1;
	// 打开串口
	ES = 1;
	// 开启中断
	EA = 1;
	
	P1 = 0xF9;
	
	while(1);
}

// 串口中断函数
void Serial() interrupt 4
{	
	// 是否收到信息
	if(RI == 1)
	{
		info = SBUF;
		RI = 0;
		// 接收到的是主机请求信息
		if(RB8 == 1)
		{
			if(info == Machine)
			{
				// 可以接受数据
				SM2 = 0;
				SBUF = Machine;
			}
		}
		// 接收到的是从机发送的数据信息
		else
		{
			// 等待接收地址帧
			SM2 = 1;
			// 将信息显示到数码管上
			P2 = info;
		}
	}
	// 信息发送完毕，清除发送中断标志位
	if(TI == 1)
	{
		TI = 0;
	}
}
	