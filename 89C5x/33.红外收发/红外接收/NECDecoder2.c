/****************************************
*@文件: NEC.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-25
*@描述: 采用NEC协议的红外接收器
*@接线: P1口控制7段数码管的段显示
*       P2.0-P2.3控制数码管的位选择
*       P3.2用于红外信号传输
****************************************/

#include <REGX52.H>
//#include <intrins.h>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

// 七段数码管
#define SEG P1
#define POS P2
#define TRUE 1
#define FALSE 0
#define TIME 100

// 红外接收头数据线
sbit IRIN = P3^2;
sbit LED = P3^0;
uchar DECODE_OK = FALSE;

// 接收到的数据
uchar addr1 = 0, addr2 = 0, dat1 = 0, dat2 = 0;

// 全局变量
// LED显示字模 0-F 共阳模式
uchar code table[]= {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};
// 函数声明
void delay(uint);
void display(uchar, uchar, uchar, uchar);
uchar getcode(void);

/****************************************
*@描述: 主函数
****************************************/
void main(void)
{
	uchar c1 = 0, c2 = 0, c3 = 0, c4 = 0;
	
	uchar i = 0;
	// 初始化外部中断0
	EX0 = 1;
	// 负边沿触发
	TCON = 0x01;
	// 打开中断开关
	EA = 1;
	// 
	IRIN = 1;
		
	while(1)
	{
		if(DECODE_OK == TRUE)
		{
			
			//c1 = dat1 / 10;
			//c2 = dat1 % 10;
			c1 = (dat1 >> 4) & 0x0F;
			c2 = dat1 & 0x0F;
			
			c3 = (addr1 >> 4) & 0x0F;
			c4 = addr1 & 0x0F;
		}

		display(c1, c2, c3, c4);
	}
}

/****************************************
*@描述: 7段数码管字符显示
*@返回: void
****************************************/
void display(uchar d1, uchar d2, uchar d3, uchar d4)
{
	// 显示低位
	POS = 0xFB;
	SEG = table[d1];
	delay(TIME);
	
	// 显示高位
	POS = 0xF7;
	SEG = table[d2];
	delay(TIME);
	
	// 地址位
	POS = 0xFE;
	SEG = table[d3];
	delay(TIME);
	
	// 地址位
	POS = 0xFD;
	SEG = table[d4];
	delay(TIME);
}

/****************************************
*@描述: 外部中断0中断函数
*@返回: void
****************************************/
void int0() interrupt 0 using 1
{
	uchar d1 = 0, d2 = 0, d3 = 0, d4 = 0;
	// 关闭中断
	EA = 0;
	
	POS = 0xFF;
	
	//DECODE_OK = FALSE;
	
	// 延时3ms
	delay(275);
	//LED = 0;
	// 不是有效的信号，丢弃
	if(IRIN == 1)
	{
		EA = 1;
		//DECODE_OK = FALSE;
		return;
	}
	
	// 跳过9ms的引导码
	while(!IRIN);
	// 跳过4.5ms的起始码, 延时4.55ms
	while(IRIN);
//	delay(418);
//	// 不是有效的信号，丢弃
//	if(IRIN == 1)
//	{
//		EX0 = 1;
//		return;
//	}
//	LED = 0;
	
	// 接收4组数据
	d1 = getcode();
	d2 = getcode();
	d3 = getcode();
	d4 = getcode();	
	
	// 2.5ms结束码
	//delay(228);
	while(!IRIN);
	
	// 校验是否为错码
	if(d3 != ~d4)
	{
		DECODE_OK = FALSE;
		EA = 1;
		return;
	}
	
	addr1 = d1;
	addr2 = d2;
	dat1 = d3;
	dat2 = d4;
	
	DECODE_OK = TRUE;
	// 重开中断，等待下一次接收数据
	EA = 1;
}

/****************************************
*@描述: 获得每组8位数据
*@参数: void
*@返回: unsigned char
****************************************/
uchar getcode(void)
{
	uchar i, temp = 0;
	// 每组数据的8位
	for(i = 0; i < 8; i++)
	{
		temp >>= 1;
		
		// 等待低电平
		while(!IRIN);
		// 延时800微秒，判断是0还是1
		// 为0高电平维持0.565毫秒
		// 为1高电平维持1.685毫秒
		// 跳过800微秒，要么还是高电平，要么已经是下一个位的低电平了
		delay(73);
		
		temp &= 0x7F;
		if(IRIN)
		{
			// 低位在前，高位在后
			temp |= 0x80;
			// 等待跳变为低电平
			while(IRIN);
		}
	}
	return temp;
}

/****************************************
*@描述: 微秒延时函数,
*       此函数精确计算：time=16.28+10.85*t (us)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	uint i;
	for(i = 0; i < t; i++);
}
