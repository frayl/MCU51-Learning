/****************************************
*@文件: NECEncoder.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-25
*@描述: 使用NEC协议实现红外发送
*@接线: P1口连接4×4键盘
*       P0.0口实现按键时LED亮灯
*       P0.7口连接红外IROUT端
****************************************/

#include <REGX52.H>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

#define KEY_PORT P1
#define ADDRESS 0xC8
#define WH 1
#define WL 0

// 全局变量
sbit LED = P0^0;
sbit IROUT = P0^7;
bit STATE = WL;

// 键盘扫描码表
uchar code keyboard[] = {
	0xEE, 0xDE, 0xBE, 0x7E,
	0xED, 0xDD, 0xBD, 0x7D,
	0xEB, 0xDB, 0xBB, 0x7B,
	0xE7, 0xD7, 0xB7, 0x77
};

// 函数声明
void sendByte(uchar);
void sendBit0(void);
void sendBit1(void);
void makeSignal(uint);
void delay(uint);
uchar keyScan(void);

/****************************************
*@描述: 
*@参数: 
*@返回:
****************************************/
void main(void)
{
	uchar key, dat, rdat;
	uchar addr = ADDRESS, raddr;
	
	while(1)
	{
		key = keyScan();
		//key = 15;
		
		// 发送编码
		if(key < 16)
		{
			dat = keyboard[key];
			rdat = ~dat;
			raddr = ~addr;
			
			// 发送引导码
			IROUT = WL;
			STATE = WL;
			// 延时9ms
			makeSignal(346);
			
			// 发送起始码
			IROUT = WH;
			STATE = WH;
			// 延时4.5ms
			makeSignal(173);
			
			// 发送地址低字节
			sendByte(addr);
			// 发送地址高字节
			sendByte(raddr);
			// 发送按键数字码
			sendByte(dat);
			// 发送按键数字反码
			sendByte(rdat);
			
			// 发送结束码
			IROUT = WL;
			STATE = WL;
			// 延时2.5ms
			makeSignal(96);
			
			IROUT = WH;
		}
	}
}

/****************************************
*@描述: 发送一个字节
*@参数: unsigned char byte, 待发送的字节
*@返回: void
****************************************/
void sendByte(uchar byte)
{
	uchar i;
	for(i = 0; i < 8; i++)
	{
		// 最低位为1
		if((byte & 0x01) == 0x01)
		{
			sendBit1();
		}
		// 最低位为0
		else
		{
			sendBit0();
		}
		// 左移一位
		byte >>= 1;
	}
}



/****************************************
*@描述: 发送编码0
*@参数: void
*@返回: void
****************************************/
void sendBit0(void)
{
	IROUT = WL;
	STATE = WL;
	// 延时0.56ms
	makeSignal(21);
	
	IROUT = WH;
	STATE = WH;
	// 延时0.565ms
	makeSignal(22);
}

/****************************************
*@描述: 发送编码1
*@参数: void
*@返回: void
****************************************/
void sendBit1(void)
{
	IROUT = WL;
	STATE = WL;
	//STATE = WL;
	// 延时0.56ms
	makeSignal(21);

	IROUT = WH;
	STATE = WH;
	// 延时1.685ms
	makeSignal(65);
}

/****************************************
*@描述: 产生17/9的PWM脉冲
*       38KHz载波每个波长的周期为26us
*       11.0592MHz的单片机，每个机器周期为1.08us
*@参数: unsigned int t
*@返回: void
****************************************/
void makeSignal(uint t)
{	
	uchar i;
	// 根据电平特性，生成不同状态的载波
	if(STATE == WH)
	{
		// 电平持续时长
		while(t--)
		{
			// 输出为低电平，保持不变
			i = 0;
			i = 0;
			i = 0;
			i = 0;
		}
		return;
	}
	else
	{
		// 电平持续时长
		while(t--)
		{
			// 让高电平持续一点时间
			i = 0;
			i = 0;			
			
			// 变为低电平持续一点时间
			IROUT = WH;
			i = 0;
			i = 0;
			
			IROUT = STATE;
		}
		return;
	}
}

/****************************************
*@描述: 微秒延时函数, time=18+6*(t-1)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	while(--t);
}

/****************************************
*@描述: 线反转法键盘扫描函数
*@参数: void
*@返回: unsigned char
****************************************/
uchar keyScan(void)
{
	uchar keycode, row, col;
	
	KEY_PORT = 0x0F;
	
	// 是否有键被按下
	if((KEY_PORT & 0x0F) != 0x0F)
	{
		LED = 0;
		// 	延时5ms
		delay(850);
		// 确实有键被按下
		if((KEY_PORT & 0x0F) != 0x0F)
		{
			row = KEY_PORT & 0x0F;
			
			// 列值
			KEY_PORT = 0xF0;
			col = KEY_PORT & 0xF0;
			
			// 按键弹起
			while(KEY_PORT != 0xF0);
			LED = 1;
		}
	}
	// 扫描码
	keycode = col | row;
	// 获得按键码
	for(col = 0; col < 16; col++)
	{
		if(keyboard[col] == keycode)
		{		
			return col;
		}
	}
	
	return 16;
}
