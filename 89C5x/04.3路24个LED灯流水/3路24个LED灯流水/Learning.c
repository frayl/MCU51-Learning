#include <REGX52.H>

#define unit unsigned int
#define uchar unsigned char

// Ƭѡ״̬
sbit p2_1 = P2^0;
sbit p2_2 = P2^1;
sbit p2_3 = P2^2;

uchar LIGHT = 0x0F;	// ÿ����յ����
uchar state = 0;
uchar temp = 0;

const int timer = 83;
int i = 0;

void delay(unit t);
uchar display(uchar d);

void main(void)
{
	p2_1 = p2_2 = p2_3 = 0;
	
	while(1)
	{
		// ��ʾ��1��LED
		p2_1 = 1;
		P1 = display(1);
		p2_1 = 0;
		
		// ��ʾ��2��LED
		p2_2 = 1;
		P1 = display(2);
		p2_2 = 0;
		
		// ��ʾ��3��LED
		p2_3 = 1;
		P1 = display(3);
		p2_3 = 0;
		
		// ��ʱ50����
		delay(200);
		
		// ѭ������
		temp = LIGHT >> 7;
		temp &= 0x01;
		LIGHT <<= 1;
		LIGHT |= temp;
	}
}

uchar display(uchar d)
{
	switch(d)
	{
		case 1:
			state = LIGHT;
			break;
		case 2:
			temp = LIGHT >> 7;
			temp &= 0x01;
			state <<= 1;
			state |= temp;
			break;
		case 3:
			temp = LIGHT >> 6;
			temp &= 0x03;
			state <<= 2;
			state |= temp;
			break;
	}
	return state;	
}

void delay(unit t)
{
	while(t--)
	{
		for(i = timer; i>=0; i--); //1����
	}
}