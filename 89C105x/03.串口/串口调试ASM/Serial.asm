;**********************************************
;*@File: Serial.asm
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-14
;*@Description: 串口调试程序设计
;*              通过串口给单片机发送字符
;*              单片机将字符发送回来
;**********************************************
	ORG 0000H
	LJMP MAIN
	ORG 0023H
	LJMP SERIAL
	ORG 0100H
MAIN:
	;初始化寄存器
	MOV SP, 04FH
	CLR RS0
	CLR RS1
	;初始化定时器T1,定时器T1为工作方式2
	ORL TMOD, #020H
	;晶振频率为11.0592MHZ，波特率为9600，不启用波特率倍增
	MOV TH1, #0FDH
	MOV TL1, #0FDH
	;禁止定时器中断
	CLR ET1
	;启动定时器T1
	SETB TR1
	;初始化串口
	MOV SCON, #050H
	;打开串口中断
	SETB ES
	;打开中断开关
	SETB EA	
	
	SJMP $

	LJMP EXIT

;串口中断子程序
SERIAL:
	PUSH PSW
	PUSH ACC
SEND:
	JBC TI, QUIT
RECEIVE:
	;测试是否收到数据，没收到则测试是否发送完数据
	JNB RI, QUIT
	;接收串口收到的数据
	MOV A, SBUF
	;清除接收中断标志
	CLR RI
	;将接收到的数据通过串口发送出去
	MOV SBUF, A
QUIT:
	POP ACC
	POP PSW
	RETI

;退出程序
EXIT:
	END
