/****************************************
*@文件: 7SEG.c
*@作者: Scriptfan <scriptfan@hotmail.com>
*@日期: 2017-01-31
*@描述: 采用NEC协议的红外接收器
*@接线: P1口控制7段数码管的段显示
*       P3.0-P3.3控制数码管的位选择
****************************************/

#include <REGX52.H>

// 宏定义
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

// 七段数码管
#define SEG P1
#define POS P3
#define TIME 100

// 全局变量
// LED显示字模 0-F 共阳模式
uchar code table[]= {
	0xC0,0xF9,0xA4,0xB0,
	0x99,0x92,0x82,0xF8,
	0x80,0x90,0x88,0x83,
	0xC6,0xA1,0x86,0x8E
};
// 函数声明
void delay(uint);
void display(uchar, uchar, uchar, uchar);

/****************************************
*@描述: 主函数
****************************************/
void main(void)
{
	while(1)
	{
		display(3, 15, 7, 2);
	}
}

/****************************************
*@描述: 7段数码管字符显示
*@返回: void
****************************************/
void display(uchar d1, uchar d2, uchar d3, uchar d4)
{
	// 显示低位
	POS = 0xFB;
	SEG = table[d1];
	delay(TIME);
	
	// 显示高位
	POS = 0xF7;
	SEG = table[d2];
	delay(TIME);
	
	// 地址位
	POS = 0xFE;
	SEG = table[d3];
	delay(TIME);
	
	// 地址位
	POS = 0xFD;
	SEG = table[d4];
	delay(TIME);
}

/****************************************
*@描述: 微秒延时函数,
*       此函数精确计算：time=16.28+10.85*t (us)
*@参数: unsigned int
*@返回: void
****************************************/
void delay(uint t)
{
	uchar i;
	for(i = 0; i < t; i++);
}
