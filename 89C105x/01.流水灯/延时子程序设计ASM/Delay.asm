;**********************************************
;*@File: Delay.asm
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-11
;*@Description: 用汇编语言设计单片机延时子程序
;*              延时100ms
;*              控制流水灯滚动
;**********************************************
	ORG 0000H
	LJMP MAIN
	ORG 0100H
MAIN:
	MOV A, #0FEH
LOOP:
	;LED灯流水滚动
	MOV P1, A
	RL A
	;调用延时子程序
	LCALL DELAY100
	SJMP LOOP

;延时子程序，延时100ms
DELAY100:
	PUSH PSW
	PUSH ACC
	;外部循环初值
	MOV R7, #0C8H
LOOP1:	
	;内部循环初值
	MOV R6, #0FAH
LOOP2:
	DJNZ R6, LOOP2
	DJNZ R7, LOOP1
	POP ACC
	POP PSW
	RET
	;程序退出
	END
