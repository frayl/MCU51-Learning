;**********************************************
;*@File: INT0.ASM
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-12
;*@Description: 用汇编语言实现外部中断控制
;*              P1口控制8个LED灯
;**********************************************
	TM1 EQU 0F8H
	TM2 EQU 0FAH
	PARAM EQU 030H
	ORG 0000H
	LJMP MAIN
	ORG 0003H
	LJMP INTR0
	ORG 0100H
MAIN:
	;LED状态控制
	STA BIT 00H
	;设置栈底
	MOV SP, #05FH
	;设置主程序工作寄存器组为0
	CLR RS0
	CLR RS1
	CLR STA
	;初始化外部中断0,低电平触发
	CLR IT0
	SETB EX0
	SETB EA
	;LED灯初识状态
	MOV A, #0FEH
START:	
	MOV P1, A
	;如果STA=0则LED灯一直点亮
	;延时200ms
	MOV PARAM, #TM1
	LCALL DELAY
	JNB STA, START
	;如果STA=1则LED灯滚动
	RL A
	SJMP START
	
	LJMP EXIT

;延时函数
DELAY:
	PUSH PSW
	PUSH ACC
	;使用工作寄存器组1	
	CLR RS0
	SETB RS1
	;接收传入参数
	MOV R2, PARAM
LOOP1:
	MOV R3, #TM2
LOOP2:
	DJNZ R3, LOOP2
	DJNZ R2, LOOP1
	POP ACC
	POP PSW
	RET

;外部中断0入口子程序
INTR0:
	PUSH PSW
	PUSH ACC
	;防止重复中断触发
	CLR EA
	CPL STA
	SETB EA
	POP ACC
	POP PSW
	RETI

;程序退出
EXIT:
	END
