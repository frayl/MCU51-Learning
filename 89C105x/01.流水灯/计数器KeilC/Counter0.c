// 标题：计数器仿真实验
// 材料：P1口连接8个LED灯
// 功能：计数器累积的数值在P1口输出
#include <REG1051.H>

char num = 0xFF;

void main(void)
{
	TMOD = 0x05;
	//设置计数器初值，每计数5次的时候中断
	TH0 = 0xFF;
	TL0 = 0xFE;
	ET0 = 1;
	EA = 1;
	TR0 = 1;
	
	P1 = num;
	
	while(1);
}

void counter0() interrupt 1 using 1
{
	EA = 0;
	TH0 = 0xFF;
	TL0 = 0xFE;
	
	num--;
	P1 = num;
	if(num == 0)
	{
		num = 0xFF;
	}
	EA = 1;
}