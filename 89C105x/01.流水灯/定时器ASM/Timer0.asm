;**********************************************
;*@File: Timer0.asm
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-12
;*@Description: 定时器中断实现延时1s
;*              P1口控制8盏LED灯
;**********************************************
	;定时器定时时长（100×10ms）
	COUNT EQU 64H
	;P1口初始状态
	LED EQU 0FEH
	;P1口状态存放地址
	ADDR_LED EQU 030H
	;定时器运行时长存放地址
	ADDR_COUNT EQU 031H
	ORG 0000H
	LJMP MAIN
	ORG 000BH
	LJMP TIMER0
	ORG 0100H
MAIN:
	;初始化寄存器
	MOV SP, 04FH
	CLR RS0
	CLR RS1
	;初始化定时器, 10ms初值
	MOV TL0, #0FDH
	MOV TH0, #0D8H
	;定时器0工作在方式1
	ORL TMOD, #01H
	;允许定时器0中断
	SETB ET0
	;打开中断
	SETB EA
	;启动定时器0
	SETB TR0
	;初始化参数
	MOV ADDR_LED, #LED
	MOV ADDR_COUNT, #COUNT
	;初始化LED灯
	MOV P1, ADDR_LED
	SJMP $

	LJMP EXIT
	
;定时器T0子程序
TIMER0:
	PUSH PSW
	PUSH ACC
	CLR RS0
	SETB RS1
	;初始化定时器, 10ms初值
	MOV TL0, #0FDH
	MOV TH0, #0D8H
	DJNZ ADDR_COUNT, QUIT
FULL:	
	MOV ADDR_COUNT, #COUNT
	MOV A, ADDR_LED
	RL A
	MOV ADDR_LED, A
	MOV P1, ADDR_LED
QUIT:
	POP ACC
	POP PSW
	RETI

;退出程序
EXIT:
	END
