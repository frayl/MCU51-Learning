;**********************************************
;*@File: Counter0.asm
;*@Author: Scriptfan <scriptfan@hotmail.com>
;*@Date: 2017-1-13
;*@Description: 汇编语言实现单片机计数器
;*              P1口控制8个LED灯
;**********************************************
	NUM EQU 0FFH
	ADDR_NUM EQU 030H
	ORG 0000H
	LJMP MAIN
	ORG 000BH
	LJMP COUNTER0
	ORG 0100H
MAIN:
	;初始化寄存器
	MOV SP, 04FH
	CLR RS0
	CLR RS1
	MOV ADDR_NUM, #NUM
	;初始化定时器0中断, 每来一个脉冲计数一次（溢出中断）
	MOV TH0, #0FFH
	MOV TL0, #0FEH
	MOV TMOD, #05H
	SETB ET0
	SETB EA
	SETB TR0
	MOV P1, #NUM
	SJMP $

	LJMP EXIT

;计数器0子程序
COUNTER0:
	PUSH PSW
	PUSH ACC
	CLR EA
	MOV TH0, #0FFH
	MOV TL0, #0FEH
	DEC ADDR_NUM
	MOV P1, ADDR_NUM
	MOV A, ADDR_NUM
	JNZ QUIT
	MOV ADDR_NUM, #NUM
QUIT:
	SETB EA
	POP ACC
	POP PSW
	RETI

;退出程序
EXIT:
	END
